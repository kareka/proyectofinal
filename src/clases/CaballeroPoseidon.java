package clases;

import javax.swing.ImageIcon;

public class CaballeroPoseidon extends Caballero{
	 private String monstruoMarino;


	public CaballeroPoseidon(String monstruoMarino,String nombre, String armadura, String procedencia, int vida, int cosmos, Ataque[] ataque
			) {
		super(nombre, armadura, procedencia, vida, cosmos, ataque);
		this.monstruoMarino = monstruoMarino;
	}

	public String getMonstruoMarino() {
		return monstruoMarino;
	}

	public void setMonstruoMarino(String monstruoMarino) {
		this.monstruoMarino = monstruoMarino;
	}
	 
}
