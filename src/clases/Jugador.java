package clases;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import excepciones.EmailIncorrectoException;

public class Jugador {
	    private String usuario;
	    private String contrasena;
	    private String email;
	    private int puntosAcumulados;
	    private CaballeroBronce caballero;
	    
	    
		public Jugador(String usuario, String contrasena, String email) throws EmailIncorrectoException {
			super();
			this.usuario = usuario;
			this.contrasena = contrasena;
			setEmail(email);
			this.puntosAcumulados = 0;
			this.caballero = null;
		}
		public Jugador(String usuario, String contrasena, String email, int puntosAcumulados,CaballeroBronce caballero) throws EmailIncorrectoException {
			super();
			this.usuario = usuario;
			this.contrasena = contrasena;
			setEmail(email);
			this.puntosAcumulados = 0;
			this.caballero = caballero;
		}

		public String getUsuario() {
			return usuario;
		}


		public void setUsuario(String usuario) {
			this.usuario = usuario;
		}


		public String getContrasena() {
			return contrasena;
		}


		public void setContrasena(String contrasena) {
			this.contrasena = contrasena;
		}


		public String getEmail() {
			return email;
		}

      /*Comprobamos que el email introducido es correcto, o sea contiene la @, sino lanzamos una excepci�n*/
		public void setEmail(String email) throws EmailIncorrectoException {
			int encontrado=email.indexOf('@');
			if(encontrado!=-1) {
				this.email = email;
			}else {
			
			throw new EmailIncorrectoException("No es una direcci�n de correo correcta");
			}
		
		}

		public int getPuntosAcumulados() {
			return puntosAcumulados;
		}


		public void setPuntosAcumulados(int puntosAcumulados) {
			this.puntosAcumulados = puntosAcumulados;
		}


		public CaballeroBronce getCaballero() {
			return caballero;
		}


		public void setCaballero(CaballeroBronce caballero) {
			this.caballero = caballero;
		}

      
		
	    
	    
	    
	    
}
