package clases;

public class Ataque {
    
   private String nombre;
   private String tipo;    
   private int nivelDano; 
   private int gastoCosmos;

   public Ataque(String nombre, String tipo, int nivelDano, int gastoCosmos) {
	super();
	this.nombre = nombre;
	this.tipo = tipo;
	this.nivelDano = nivelDano;
	this.gastoCosmos = gastoCosmos;
}

public String getNombre() {
	return nombre;
}

public void setNombre(String nombre) {
	this.nombre = nombre;
}

public String getTipo() {
	return tipo;
}

public void setTipo(String tipo) {
	this.tipo = tipo;
}

public int getNivelDano() {
	return nivelDano;
}

public void setNivelDano(int nivelDano) {
	this.nivelDano = nivelDano;
}

public int getGastoCosmos() {
	return gastoCosmos;
}

public void setGastoCosmos(int gastoCosmos) {
	this.gastoCosmos = gastoCosmos;
}
   
   
}
