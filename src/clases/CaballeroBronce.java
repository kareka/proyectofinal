package clases;

import javax.swing.ImageIcon;

public class CaballeroBronce extends Caballero{
	  private String constelacion;
	  private String historia;
 
	

	public CaballeroBronce(String constelacion, String historia,String nombre, String armadura, String procedencia, int vida, int cosmos, Ataque[] ataque
			) {
		super(nombre, armadura, procedencia, vida, cosmos, ataque);
		this.constelacion = constelacion;
		this.historia = historia;
	}
	public String getConstelacion() {
		return constelacion;
	}
	public void setConstelacion(String constelacion) {
		this.constelacion = constelacion;
	}
	public String getHistoria() {
		return historia;
	}
	public void setHistoria(String historia) {
		this.historia = historia;
	}
	  
	  
}
