package clases;

import java.util.ArrayList;

public class Escenario {
	private String nombre;
    private ArrayList<Caballero> caballeros;
	
    

	public Escenario(String nombre, ArrayList<Caballero> caballeros) {
		super();
		this.nombre = nombre;
		this.caballeros = caballeros;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<Caballero> getCaballeros() {
		return caballeros;
	}

	public void setCaballeros(ArrayList<Caballero> caballeros) {
		this.caballeros = caballeros;
	}
	
	
    
    
}
