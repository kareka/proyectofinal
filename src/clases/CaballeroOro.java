package clases;

import javax.swing.ImageIcon;

public class CaballeroOro extends Caballero{
	 private String casa;		


	public CaballeroOro(String casa,String nombre, String armadura, String procedencia, int vida, int cosmos, Ataque[] ataque
			) {
		super(nombre, armadura, procedencia, vida, cosmos, ataque);
		this.casa = casa;
	}

	public String getCasa() {
		return casa;
	}

	public void setCasa(String casa) {
		this.casa = casa;
	}
	 
}
