package clases;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JTextPane;

import interfaces.Ventana;

public class Caballero {
   private String nombre;
   private String armadura;
   private String procedencia;    
   private int vida;
   private int vidaInicial;
   private int cosmos;
   private int cosmosInicial;
   private Ataque[]ataque;

   
public Caballero(String nombre, String armadura, String procedencia, int vida, int cosmos,
		Ataque[] ataque) {
	super();
	this.cosmosInicial=cosmos;
	this.nombre = nombre;
	this.armadura = armadura;
	this.procedencia = procedencia;
	this.vida = vida;
	this.vidaInicial=vida;
	this.cosmos = cosmos;
	this.ataque = ataque;
}


public String getNombre() {
	return nombre;
}


public void setNombre(String nombre) {
	this.nombre = nombre;
}


public String getArmadura() {
	return armadura;
}


public void setArmadura(String armadura) {
	this.armadura = armadura;
}


public String getProcedencia() {
	return procedencia;
}


public void setProcedencia(String procedencia) {
	this.procedencia = procedencia;
}


public int getVida() {
	return vida;
}


public void setVida(int vida) {
	this.vida = vida;
}


public int getCosmos() {
	return cosmos;
}


public void setCosmos(int cosmos) {
	this.cosmos = cosmos;
}


public Ataque[] getAtaque() {
	return ataque;
}


public void setAtaque(Ataque[] ataque) {
	this.ataque = ataque;
}
   
public int getVidaInicial() {
	return vidaInicial;
}


public void setVidaInicial(int vidaInicial) {
	this.vidaInicial = vidaInicial;
}


public int getCosmosInicial() {
	return cosmosInicial;
}


public void setCosmosInicial(int cosmosInicial) {
	this.cosmosInicial = cosmosInicial;
}



/*Estas son las funciones necesarias para el desarrollo de nuestro juego*/  
/*Esta funci�n nos devolver� aleatoriamente un rival */ 
  public static  Caballero  eligeRival(ArrayList<Caballero> caballeros){    
	  Caballero rival=null;
	  int numero= (int) (Math.random()*(caballeros.size())+0);  
      rival=caballeros.get(numero);  
      caballeros.remove(numero);
      
	    return rival;  
	  }   
	 
 
  
  public enum ResultadoBatalla{
	  RIVALMUERTO,
	  ATAQUEFALLIDO,
	  CONTINUAR
  }
  
 /*Esta funci�n nos ofrece el ataque de tipo golpe de nuestro personaje*/
  public ResultadoBatalla ataqueGolpe(Caballero jugador2 , Caballero jugador1) {	 
	
	  if((getVida()>0)&&(jugador2.getVida()>0)){		  		  
		  if(getCosmos()>=20) {			  
			  int numero = (int) (Math.random()*2+1);
			     if(numero==1) {
			           jugador2.setVida(jugador2.getVida()-20);
			           jugador1.setCosmos(jugador1.getCosmos()-20);
			           if(jugador2.getVida()<=0) {
			        	   
			        	   return ResultadoBatalla.RIVALMUERTO;
			           }
			           
			    	 
			     }else{
			    	 return ResultadoBatalla.ATAQUEFALLIDO;
			    	
			     }
			  
		  }else {
			  
			  setCosmos(cosmosInicial);
			  
		  }
	 }
	  	 
	  return ResultadoBatalla.CONTINUAR;
	  
  }
  



  
  /*Esta funci�n nos ofrece el ataque de tipo poder de nuestro personaje*/
  public ResultadoBatalla ataquePoder(Caballero jugador2, Caballero jugador1) {	 
		 
		  if((getVida()>0)&&(jugador2.getVida()>0)){		  		  
			  if(getCosmos()>=30) {			  
				  int numero = (int) (Math.random()*2+1);
				     if(numero==1) {
				           jugador2.setVida(jugador2.getVida()-30);	
				           jugador1.setCosmos(jugador1.getCosmos()-30);
				           if(jugador2.getVida()<=0) {
				        	   return ResultadoBatalla.RIVALMUERTO;
				           }
				           
				    	 
				     }else{
				    	 return ResultadoBatalla.ATAQUEFALLIDO;
				    	
				     }
				  
			  }else {
				  setCosmos(cosmosInicial);
				  
			  }
		 }
		  return ResultadoBatalla.CONTINUAR;
	  }
	  
	 
  
  /*Esta funci�n nos ofrece el ataque de tipo poder de nuestro personaje*/
  public ResultadoBatalla ataqueFinal(Caballero jugador2, Caballero jugador1) {	 
		 
		  if((getVida()>0)&&(jugador2.getVida()>0)){		  		  
			  if(getCosmos()>=50) {			  
				  int numero = (int) (Math.random()*2+1);
				     if(numero==1) {
				           jugador2.setVida(jugador2.getVida()-50);
				           jugador1.setCosmos(jugador1.getCosmos()-50);
				           if(jugador2.getVida()<=0) {
				        	   return ResultadoBatalla.RIVALMUERTO;
				           }
				           
				    	 
				     }else{
				    	 return ResultadoBatalla.ATAQUEFALLIDO;
				    	
				     }
				  
			  }else {
				  setCosmos(cosmosInicial);
				  
			  }
		 }
		  return ResultadoBatalla.CONTINUAR;
	  }
	  
	
  public static  void ataqueRival(int cosmosInicial,Caballero jugador,Caballero rival,JTextPane panelJuego2,JProgressBar vidaJugador
		  ,JProgressBar cosmosRival) {
	  if((jugador.getVida()>0)&&(rival.getVida()>0)){
		  int numero = (int) (Math.random()*4+1);
		  switch(numero) {
		  case 1:
			  
			  if(rival.getCosmos()>=20) {				  
				   numero = (int) (Math.random()*2+1);
				     if(numero==1) {
				       if(jugador.getVida()>=20) {	 
				         jugador.setVida(jugador.getVida()-20);
				         vidaJugador.setValue(jugador.getVida());
				         vidaJugador.getValue();
				         rival.setCosmos(rival.getCosmos()-20);
				         cosmosRival.setValue(rival.getCosmos());
				         cosmosRival.getValue();
				         panelJuego2.setText(rival.getNombre()+" invoca "+rival.getAtaque()[0].getNombre()+"\nLa vida de "+jugador.getNombre()+" es "+jugador.getVida());
				         panelJuego2.getText();
				         if(jugador.getVida()<=0) {
				        	 
				        	 panelJuego2.setText(rival.getNombre()+" invoca "+rival.getAtaque()[0].getNombre()+"\nEl jugador "+jugador.getNombre()+" ha sido derrotado, "+rival.getNombre()+" es el vencedor.");
				        	 panelJuego2.setText("Si quieres continuar pulsa cualquier tecla de ataque.");
				        	 jugador.setVida(jugador.getVidaInicial());
							 jugador.setCosmos(jugador.getCosmosInicial());						   
							 rival.setVida(rival.getVidaInicial());
							 rival.setCosmos(rival.getCosmosInicial());
						    
						     
				         }
				       }else {
				    	   
					       panelJuego2.setText(rival.getNombre()+" invoca "+rival.getAtaque()[0].getNombre()+"\nEl jugador "+jugador.getNombre()+" ha sido derrotado, "+rival.getNombre()+" es el vencedor.");
					       panelJuego2.setText("Si quieres continuar pulsa cualquier tecla de ataque.");
					       jugador.setVida(jugador.getVidaInicial());
						   jugador.setCosmos(jugador.getCosmosInicial());						   
						   rival.setVida(rival.getVidaInicial());
						   rival.setCosmos(rival.getCosmosInicial());
					       
						   
					       					     
				       }
				     }
				     else {
				    	 
				    	 panelJuego2.setText(rival.getNombre()+" invoca "+rival.getAtaque()[0].getNombre()+"\nEl ataque ha fallado");
				    	 panelJuego2.getText();
				    	 rival.setCosmos(rival.getCosmos()-20);
				    	 cosmosRival.setValue(rival.getCosmos());
				         cosmosRival.getValue();
				     }
				  
			  }else {
				  rival.setCosmos(cosmosInicial);
				  panelJuego2.setText("No tienes suficiente cosmos para atacar.");
				  panelJuego2.getText();
				  
			  }
		   
		  break;
		  
		  case 2:
			 
			  if(rival.getCosmos()>=30) {
				   numero = (int) (Math.random()*2+1);
				     if(numero==1) {
				       if(jugador.getVida()>=30) { 
				         jugador.setVida(jugador.getVida()-30);
				         vidaJugador.setValue(jugador.getVida());
				         vidaJugador.getValue();
				         rival.setCosmos(rival.getCosmos()-30);
				         cosmosRival.setValue(rival.getCosmos());
				         cosmosRival.getValue();
				         panelJuego2.setText(rival.getNombre()+" invoca "+rival.getAtaque()[1].getNombre()+"\nLa vida de "+jugador.getNombre()+" es "+jugador.getVida());
				         panelJuego2.getText();
				         if(jugador.getVida()<=0) {
				        	 
				        	 panelJuego2.setText(rival.getNombre()+" invoca "+rival.getAtaque()[1].getNombre()+"\nEl jugador "+jugador.getNombre()+" ha sido derrotado, "+rival.getNombre()+" es el vencedor.");
				        	 panelJuego2.setText("Si quieres continuar pulsa cualquier tecla de ataque.");
				        	 jugador.setVida(jugador.getVidaInicial());
						     jugador.setCosmos(jugador.getCosmosInicial());				     
						     rival.setVida(rival.getVidaInicial());
						     rival.setCosmos(rival.getCosmosInicial());
						     
				         }
				       }else {
				    	     
					     panelJuego2.setText(rival.getNombre()+" invoca "+rival.getAtaque()[1].getNombre()+"\nEl jugador "+jugador.getNombre()+" ha sido derrotado, "+rival.getNombre()+" es el vencedor.");	
					     panelJuego2.setText("Si quieres continuar pulsa cualquier tecla de ataque.");
					     jugador.setVida(jugador.getVidaInicial());
					     jugador.setCosmos(jugador.getCosmosInicial());				     
					     rival.setVida(rival.getVidaInicial());
					     rival.setCosmos(rival.getCosmosInicial());
					     
					     					     
				      }
				     }
				     else {
				    	
				    	 panelJuego2.setText(rival.getNombre()+" invoca "+rival.getAtaque()[1].getNombre()+"\nEl ataque ha fallado");
				    	 panelJuego2.getText();
				    	 rival.setCosmos(rival.getCosmos()-30);
				    	 cosmosRival.setValue(rival.getCosmos());
				         cosmosRival.getValue();
				     }
				  
			  }else {
				  rival.setCosmos(cosmosInicial);
				  panelJuego2.setText("No tienes suficiente cosmos para atacar.");
				  panelJuego2.getText();
				  
			  }
		  break;
		  
		  case 3:
			  
			  if(rival.getCosmos()>=50) {				  
				   numero = (int) (Math.random()*2+1);
				   if(numero==1) {
				    if(jugador.getVida()>=50) {	 
				       jugador.setVida(jugador.getVida()-50);
				       vidaJugador.setValue(jugador.getVida());
				       vidaJugador.getValue();
				       rival.setCosmos(rival.getCosmos()-50);
				       cosmosRival.setValue(rival.getCosmos());
				       cosmosRival.getValue();
				       panelJuego2.setText(rival.getNombre()+" invoca "+rival.getAtaque()[2].getNombre()+"\nLa vida de "+jugador.getNombre()+" es "+jugador.getVida());
				       panelJuego2.getText();
				       if(jugador.getVida()<=0) {
				    	     
				        	 panelJuego2.setText(rival.getNombre()+" invoca "+rival.getAtaque()[2].getNombre()+"\nEl jugador "+jugador.getNombre()+" ha sido derrotado, "+rival.getNombre()+" es el vencedor.");				        	 
				        	 panelJuego2.setText("Si quieres continuar pulsa cualquier tecla de ataque.");
				        	 jugador.setVida(jugador.getVidaInicial());
						     jugador.setCosmos(jugador.getCosmosInicial());						     
						     rival.setVida(rival.getVidaInicial());
						     rival.setCosmos(rival.getCosmosInicial());
				         }
				     }else {
				    	 
					     panelJuego2.setText(rival.getNombre()+" invoca "+rival.getAtaque()[2].getNombre()+"\nEl jugador "+jugador.getNombre()+" ha sido derrotado, "+rival.getNombre()+" es el vencedor.");
					     panelJuego2.setText("Si quieres continuar pulsa cualquier tecla de ataque.");
					     jugador.setVida(jugador.getVidaInicial());
					     jugador.setCosmos(jugador.getCosmosInicial());					     
					     rival.setVida(rival.getVidaInicial());
					     rival.setCosmos(rival.getCosmosInicial());
					     
					    
				     }
				    }
				     else {
				    	
				    	 panelJuego2.setText(rival.getNombre()+" invoca "+rival.getAtaque()[2].getNombre()+"\nEl ataque ha fallado");
				    	 panelJuego2.getText();
				    	 rival.setCosmos(rival.getCosmos()-50);
				    	 cosmosRival.setValue(rival.getCosmos());
				         cosmosRival.getValue();
				     }
				  
			  }else {
				  rival.setCosmos(cosmosInicial);
				  panelJuego2.setText("No tienes suficiente cosmos para atacar.");
				  panelJuego2.getText();
				  
			  }  
		  break;
		  }
  }
  
} 
 

}  

