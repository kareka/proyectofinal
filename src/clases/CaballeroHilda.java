package clases;

import javax.swing.ImageIcon;

public class CaballeroHilda extends Caballero{
	private String mitologia;


	public CaballeroHilda(String mitologia,String nombre, String armadura, String procedencia, int vida, int cosmos, Ataque[] ataque
			) {
		super(nombre, armadura, procedencia, vida, cosmos, ataque);
		this.mitologia = mitologia;
	}

	public String getMitologia() {
		return mitologia;
	}

	public void setMitologia(String mitologia) {
		this.mitologia = mitologia;
	}
	
}
