package interfaces;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.Font;
import java.awt.Dimension;

public class PantallaMapa extends JPanel{
	private Ventana ventana;
	
	
	public PantallaMapa(Ventana v) {
		super();
		setSize(new Dimension(1008, 536));
		ventana=v;
		setBackground(new Color(153, 153, 255));
	  	setLayout(null);
	  	System.out.println(v.puntosConseguidos);
	 
	  	   
 	
/*En este bot�n podremos ir a la pantallla del Torneo Gal�ctico*/  	
 	JButton botonTorneo = new JButton("");
 	botonTorneo.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent arg0) {	 	  			
		     v.irPantallaTorneo();	  	
					     
		}
 	});
 	botonTorneo.setBounds(704, 110, 126, 67);
 	botonTorneo.setIcon(new ImageIcon("./imagenes/mini_torneo.jpg"));
 	add(botonTorneo);
 	
/*En este bot�n podremos ir a la pantalla del Santuario*/ 	
 	JButton botonSantuario = new JButton(""); 	 	
 	botonSantuario.setEnabled(false);	
 	botonSantuario.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
					if(v.puntosConseguidos>=200){
					   botonSantuario.setEnabled(true);	
					   v.irPantallaSantuario();
					}
		}
 	});
 	botonSantuario.setBounds(176, 90, 126, 67);
 	botonSantuario.setIcon(new ImageIcon("./imagenes/mini_santuario.jpg"));
 	add(botonSantuario);
  
   /*En este bot�n iremos a la pantalla de Asgard*/	  	
 	JButton botonAsgard = new JButton("");
 	botonAsgard.setEnabled(false);
 	botonAsgard.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
					if(v.puntosConseguidos>=450) {
					  botonAsgard.setEnabled(true);	
					  v.irPantallaAsgard();
					}
		}
 	});
 	botonAsgard.setBounds(134, 382, 126, 67);
 	botonAsgard.setIcon(new ImageIcon("./imagenes/mini_asgard.jpg"));
 	add(botonAsgard);
 	
/*En este bot�n iremos a la pantalla de Poseid�n*/  	
 	JButton botonPoseidon = new JButton("");
 	botonPoseidon.setEnabled(false);
 	botonPoseidon.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
					if(v.puntosConseguidos>=700) {
					botonPoseidon.setEnabled(true);	
					v.irPantallaPoseidon();
					}
		}
 	});
 	botonPoseidon.setBounds(610, 382, 126, 67);
 	botonPoseidon.setIcon(new ImageIcon("./imagenes/mini_templo_poseidon.jpg"));
 	add(botonPoseidon);
 	
 	JButton botonFinal = new JButton("");
 	botonFinal.addMouseListener(new MouseAdapter() {
 		@Override
 		public void mouseClicked(MouseEvent arg0) {
 			if(v.puntosConseguidos>=950) {
 				botonFinal.setEnabled(true);
 				switch(v.getJugador().getCaballero().getNombre()) {
 				case "Shiryu":
 					v.irFinalShiryu();
 				break;	
 				case "Seiya":
 					v.irFinalSeiya();
 				break;
 				case "Yoga":
 					v.irFinalYoga();
 				break;
 				case "Shun":
 					v.irFinalShun();
 				break;
 				case "Ikki":
 					v.irFinalIkki();
 				break;	
 				}
 			}
 		}
 	});
 	botonFinal.setEnabled(false);
 	botonFinal.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 18));
 	botonFinal.setBounds(409, 232, 126, 67);
 	botonFinal.setIcon(new ImageIcon("./imagenes/miniFinal.jpg"));
 	add(botonFinal);
 	
 	JLabel etiquetaTitulo = new JLabel("MAPA DE JUEGO");
 	etiquetaTitulo.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
 	etiquetaTitulo.setBounds(422, 23, 205, 33);
 	add(etiquetaTitulo); 
 	

 	JLabel lblNewLabel = new JLabel("FINAL");
 	lblNewLabel.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
 	lblNewLabel.setBounds(437, 197, 69, 27);
 	add(lblNewLabel);
 	
 	
/*Esta etiqueta lo que hace es poner una imagen de fondo en nuestro mapa*/  	
 	JLabel etiqueta_mapa_juego = new JLabel("");
 	etiqueta_mapa_juego.setBounds(0, 0, 1008, 536);
 	etiqueta_mapa_juego.setIcon(new ImageIcon("./imagenes/mapa_juego2.jpg"));
 	add(etiqueta_mapa_juego);
 	
 	
 	
  
				
	 }	
	}

