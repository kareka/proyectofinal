package interfaces;

import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

import clases.Caballero;
import clases.CaballeroPoseidon;
import clases.Escenario;
import clases.Caballero.ResultadoBatalla;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.awt.Dimension;

public class PantallaPoseidon extends JPanel{
	private Ventana ventana;
	private PantallaPoseidon thisRef;
	private Caballero jugador2;
    public PantallaPoseidon (Ventana v) {
  	  super();
  	  thisRef=this;
  	  setSize(new Dimension(1008, 536));
  	  ventana=v;
  	  setBackground(new Color(153, 153, 255));
  	  setLayout(null);
  	
  	ArrayList<Caballero> caballeroPoseidon=new ArrayList<Caballero>();
  	caballeroPoseidon.add(v.caballeroPoseidonKraken);
  	caballeroPoseidon.add(v.caballeroPoseidonSirena);
  	caballeroPoseidon.add(v.caballeroPoseidonChrysaor);
  	caballeroPoseidon.add(v.caballeroPoseidonCaballoMarino);
  	caballeroPoseidon.add(v.caballeroPoseidonDragonDeLosMares);
  	
  	 /*Esta es la primera pantalla de combate de nuestro juego*/
  	 /*Asignamos una variable para mi caballero y el rival*/ 	  
  	  	  Caballero jugador1=v.getJugador().getCaballero();
  	  	  jugador2=Caballero.eligeRival(caballeroPoseidon);
  	  	  jugador1.setVidaInicial(200);
	  	  jugador1.setCosmosInicial(200);
	  	  
  	  	  
  	  	  
  	  	  
  	/*Este panel nos mostrar� el desarrollo de la lucha ataque a ataque*/	
  	  JTextPane panelJuego = new JTextPane();
  	  panelJuego.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 22));
  	  panelJuego.setBounds(31, 170, 232, 174);	  
  	  add(panelJuego);  	  
  	 
  	/*Este panel nos mostrar� el desarrollo de los ataques del rival*/  
  	  JTextPane panelJuego2 = new JTextPane();
  	  panelJuego2.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
  	  panelJuego2.setBounds(738, 170, 232, 174);
  	  add(panelJuego2);
  	  
  	/*Aqu� aparecer�n los puntos obtenidos por cada combate ganado, que se ir�n sumando al total de nuestro usuario*/	
  	  JTextPane textoPuntos = new JTextPane();
  	  textoPuntos.setText("0 ptos");
  	  textoPuntos.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
  	  textoPuntos.setBounds(824, 41, 100, 30);
  	  add(textoPuntos);
  	  
  	  
    	/*Esta barra de progreso est� unida a la variable de vida que creamos para nuestro jugador*/	
    	  JProgressBar vidaJugador = new JProgressBar();
    	  vidaJugador.setMaximum(200);
    	  vidaJugador.setBackground(Color.WHITE);
    	  vidaJugador.setString("");
    	  vidaJugador.setStringPainted(true);
    	  vidaJugador.setValue(200);
    	  vidaJugador.setToolTipText("");
    	  vidaJugador.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));
    	  vidaJugador.setForeground(new Color(255, 0, 0));
    	  vidaJugador.setBounds(292, 368, 146, 15);
  	  add(vidaJugador);
  	  
  	  /*Esta barra de progreso est� unida a la variable de cosmos que creamos para nuestro jugador*/
  	  JProgressBar cosmosJugador = new JProgressBar();
  	  cosmosJugador.setMaximum(200);
  	  cosmosJugador.setBackground(Color.WHITE);
  	  cosmosJugador.setString("");
  	  cosmosJugador.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));
  	  cosmosJugador.setStringPainted(true);
  	  cosmosJugador.setValue(200);
  	  cosmosJugador.setForeground(new Color(153, 255, 255));
  	  cosmosJugador.setBounds(292, 396, 146, 15);
  	  add(cosmosJugador);
  	  
  	  /*Esta barra ser� la de la vida de nuestro adversario*/
  	  JProgressBar vidaRival = new JProgressBar();
  	  vidaRival.setMaximum(200);
   	  vidaRival.setStringPainted(true);
   	  vidaRival.setBackground(Color.WHITE);
   	  vidaRival.setString("");
   	  vidaRival.setValue(200);
   	  vidaRival.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));	  	  
   	  vidaRival.setForeground(new Color(255, 0, 0));
   	  vidaRival.setBounds(563, 368, 146, 15);
   	  add(vidaRival);
  	  
  	  /*Esta barra ser� la del cosmos de nuestro adversario*/
   	  JProgressBar cosmosRival = new JProgressBar();
   	  cosmosRival.setMaximum(200);
   	  cosmosRival.setStringPainted(true);
   	  cosmosRival.setBackground(Color.WHITE);
   	  cosmosRival.setValue(200);
   	  cosmosRival.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));
   	  cosmosRival.setForeground(new Color(153, 255, 255));
   	  cosmosRival.setString("");
   	  cosmosRival.setBounds(563, 396, 146, 15);	
   	  add(cosmosRival);
   	  
   	JLabel imagenLuchador2 = new JLabel("New label");
	  imagenLuchador2.setBounds(594, 126, 100, 218);
	  switch(jugador2.getNombre()) {
	  case "Isaac":
		  imagenLuchador2.setIcon(new ImageIcon("./imagenes/kraken2.jpg"));
	  break;
	  case "Baian":
		  imagenLuchador2.setIcon(new ImageIcon("./imagenes/hipocampo2.jpg"));
	  break;
	  case "Sorrento":
		  imagenLuchador2.setIcon(new ImageIcon("./imagenes/sirena2.jpg"));
	  break;
	  case "Krishna":
		  imagenLuchador2.setIcon(new ImageIcon("./imagenes/crisaor2.jpg"));
	  break;
	  case "Kanon":
		  imagenLuchador2.setIcon(new ImageIcon("./imagenes/dragon_del_mar2.jpg"));
	  break;	  
	  }
	  imagenLuchador2.setVisible(false);
	  imagenLuchador2.setVisible(true);
	  add(imagenLuchador2);
	
	  /*En este bot�n elegimos el tipo de ataque que menos da�o le har� a nuestro enemigo*/
   	 JButton botonGolpe = new JButton("GOLPE");  	  
 	  botonGolpe.addMouseListener(new MouseAdapter() {
 	  	@Override
 	  	public void mouseClicked(MouseEvent arg0) {
 	  	ResultadoBatalla rgj=jugador1.ataqueGolpe(jugador2,jugador1);
 	  		Caballero.ataqueRival(200, jugador1, jugador2, panelJuego2,vidaJugador,cosmosRival);
 	  		   vidaRival.setValue(jugador2.getVida());
	           cosmosJugador.setValue(jugador1.getCosmos());
	           panelJuego.setText(jugador1.getNombre()+" invoca "+jugador1.getAtaque()[0].getNombre()+"\nLa vida de "+jugador2.getNombre()+" es "+jugador2.getVida());
 	  		switch (rgj) {
 	  		case	RIVALMUERTO:
 	  		    v.puntosConseguidos=(v.puntosConseguidos+50);
 	  		    v.guardarPartida(50);
 	  		    panelJuego.setText(jugador1.getNombre()+" ha vencido");
 	  			JButton siguienteRival =new JButton("Rival");
 	  			siguienteRival.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 18));
  	  	        
 	  			siguienteRival.setBounds(47,10,100,30);
 	  			thisRef.add(siguienteRival);
			     textoPuntos.setText(v.puntosConseguidos+" puntos");
 	  			 siguienteRival.addMouseListener(new MouseAdapter() {

					@Override
					public void mouseClicked(MouseEvent e) {
						if(!caballeroPoseidon.isEmpty()) {
						  jugador2=Caballero.eligeRival(caballeroPoseidon);
						  switch(jugador2.getNombre()) {
						  case "Isaac":
							  imagenLuchador2.setIcon(new ImageIcon("./imagenes/kraken2.jpg"));
						  break;
						  case "Baian":
							  imagenLuchador2.setIcon(new ImageIcon("./imagenes/hipocampo2.jpg"));
						  break;
						  case "Sorrento":
							  imagenLuchador2.setIcon(new ImageIcon("./imagenes/sirena2.jpg"));
						  break;
						  case "Krishna":
							  imagenLuchador2.setIcon(new ImageIcon("./imagenes/crisaor2.jpg"));
						  break;
						  case "Kanon":
							  imagenLuchador2.setIcon(new ImageIcon("./imagenes/dragon_del_mar2.jpg"));
						  break;	  
						  }
						  imagenLuchador2.setVisible(false);
						  imagenLuchador2.setVisible(true);
						  jugador1.setVida(200);
						  jugador1.setCosmos(200);
						  
						  //Actualizar valores a todos los campos de texto relevantes y los progressbar
						  siguienteRival.setVisible(false);
						}else {
							ventana.irPantallaMapa();
						}
					}
 	  				
 	  			});
 	  			break;
 	  		case CONTINUAR:	
 	  			
 	  			break;
 	  		case ATAQUEFALLIDO:
 	  		 panelJuego.setText("El ataque ha fallado");	
	    	 jugador1.setCosmos(jugador1.getCosmos()-20);
	    	 cosmosJugador.setValue(jugador1.getCosmos());
	         cosmosJugador.getValue();
 	  		 break;
 	  		}
 	  	thisRef.setVisible(false);
 	  	thisRef.setVisible(true);
 	  	}
 	  });
 	  botonGolpe.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
 	  botonGolpe.setBounds(119, 430, 185, 32);
 	  add(botonGolpe);
  	  
  	 /*En este bot�n elegimos el tipo de ataque medio*/
 	 JButton botonPoder = new JButton("PODER");  	  
 	  botonPoder.addMouseListener(new MouseAdapter() {
 	  	@Override
 	  	public void mouseClicked(MouseEvent arg0) {
 	  	ResultadoBatalla rgj=jugador1.ataquePoder(jugador2,jugador1);
	  	   Caballero.ataqueRival(200, jugador1, jugador2, panelJuego2,vidaJugador,cosmosRival);
	  	   vidaRival.setValue(jugador2.getVida());
          cosmosJugador.setValue(jugador1.getCosmos());
          panelJuego.setText(jugador1.getNombre()+" invoca "+jugador1.getAtaque()[1].getNombre()+"\nLa vida de "+jugador2.getNombre()+" es "+jugador2.getVida());
	  		switch (rgj) {
	  		case	RIVALMUERTO:
	  			v.puntosConseguidos=(v.puntosConseguidos+50);
	  			v.guardarPartida(50);
	  			panelJuego.setText(jugador1.getNombre()+" ha vencido");
	  			JButton siguienteRival =new JButton("Rival");
	  			siguienteRival.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 18));
  	  	        
	  			siguienteRival.setBounds(47,10,100,30);
	  			thisRef.add(siguienteRival);
		        textoPuntos.setText(v.getJugador().getPuntosAcumulados()+" puntos");
	  			siguienteRival.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					if(!caballeroPoseidon.isEmpty()) {
					  jugador2=Caballero.eligeRival(caballeroPoseidon);
					  switch(jugador2.getNombre()) {
					  case "Isaac":
						  imagenLuchador2.setIcon(new ImageIcon("./imagenes/kraken2.jpg"));
					  break;
					  case "Baian":
						  imagenLuchador2.setIcon(new ImageIcon("./imagenes/hipocampo2.jpg"));
					  break;
					  case "Sorrento":
						  imagenLuchador2.setIcon(new ImageIcon("./imagenes/sirena2.jpg"));
					  break;
					  case "Krishna":
						  imagenLuchador2.setIcon(new ImageIcon("./imagenes/crisaor2.jpg"));
					  break;
					  case "Kanon":
						  imagenLuchador2.setIcon(new ImageIcon("./imagenes/dragon_del_mar2.jpg"));
					  break;	  
					  }
					  imagenLuchador2.setVisible(false);
					  imagenLuchador2.setVisible(true);
					  jugador1.setVida(200);
					  jugador1.setCosmos(200);
					  
					  //Actualizar valores a todos los campos de texto relevantes y los progressbar
					  siguienteRival.setVisible(false);
					}else {
						ventana.irPantallaMapa();
					}
				}
	  				
	  			});
	  			break;
	  		case CONTINUAR:
	  			
	  			break;
	  		case ATAQUEFALLIDO:
	  	 panelJuego.setText("El ataque ha fallado");		
   	 jugador1.setCosmos(jugador1.getCosmos()-30);
   	 cosmosJugador.setValue(jugador1.getCosmos());
        cosmosJugador.getValue();
	  		 break;
	  		}
	  	thisRef.setVisible(false);
	  	thisRef.setVisible(true);
 	  	}
 	  });
 	  botonPoder.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
 	  botonPoder.setBounds(398, 430, 185, 32);
 	  add(botonPoder);
  	  
  	/*En este bot�n elegiremos el ataque m�s letal que posee nuestro caballero*/	
 	 JButton botonFinal = new JButton("FINAL");  	  
 	  botonFinal.addMouseListener(new MouseAdapter() {
 	  	@Override
 	  	public void mouseClicked(MouseEvent e) {
 	  	ResultadoBatalla rgj=jugador1.ataqueFinal(jugador2,jugador1);
	  	   Caballero.ataqueRival(200, jugador1, jugador2, panelJuego2,vidaJugador,cosmosRival);
	  	   vidaRival.setValue(jugador2.getVida());
          cosmosJugador.setValue(jugador1.getCosmos());
          panelJuego.setText(jugador1.getNombre()+" invoca "+jugador1.getAtaque()[2].getNombre()+"\nLa vida de "+jugador2.getNombre()+" es "+jugador2.getVida());
	  		switch (rgj) {
	  		case	RIVALMUERTO:
	  			v.puntosConseguidos=(v.puntosConseguidos+50);
	  			v.guardarPartida(50);
	  			panelJuego.setText(jugador1.getNombre()+" ha vencido");
	  			JButton siguienteRival =new JButton("Rival");
	  			siguienteRival.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 18));
  	  	        
	  			siguienteRival.setBounds(47,10,100,30);
	  			thisRef.add(siguienteRival);
		        textoPuntos.setText(v.puntosConseguidos+" puntos");
	  			siguienteRival.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					if(!caballeroPoseidon.isEmpty()) {
					  jugador2=Caballero.eligeRival(caballeroPoseidon);	
					  switch(jugador2.getNombre()) {
					  case "Isaac":
						  imagenLuchador2.setIcon(new ImageIcon("./imagenes/kraken2.jpg"));
					  break;
					  case "Baian":
						  imagenLuchador2.setIcon(new ImageIcon("./imagenes/hipocampo2.jpg"));
					  break;
					  case "Sorrento":
						  imagenLuchador2.setIcon(new ImageIcon("./imagenes/sirena2.jpg"));
					  break;
					  case "Krishna":
						  imagenLuchador2.setIcon(new ImageIcon("./imagenes/crisaor2.jpg"));
					  break;
					  case "Kanon":
						  imagenLuchador2.setIcon(new ImageIcon("./imagenes/dragon_del_mar2.jpg"));
					  break;	  
					  }
					  imagenLuchador2.setVisible(false);
					  imagenLuchador2.setVisible(true);
					  jugador1.setVida(200);
					  jugador1.setCosmos(200);
					  
					  //Actualizar valores a todos los campos de texto relevantes y los progressbar
					  siguienteRival.setVisible(false);
					}else {
						ventana.irPantallaMapa();
					}
				}
	  				
	  			});
	  			break;
	  		case CONTINUAR:
	  			
	  			break;
	  		case ATAQUEFALLIDO:
	  	 panelJuego.setText("El ataque ha fallado");
   	     jugador1.setCosmos(jugador1.getCosmos()-50);
   	     cosmosJugador.setValue(jugador1.getCosmos());
         cosmosJugador.getValue();
	  		 break;
	  		}
	  	thisRef.setVisible(false);
	  	thisRef.setVisible(true);
 	  	}
 	  });
 	  botonFinal.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
 	  botonFinal.setBounds(671, 430, 185, 32);
 	  add(botonFinal);
  	  
  	
  	
	  
	  /*Estos dos botones nos sirven para poner las etiquetas de las imagenes de nuestros luchadores*/
	  JButton luchador1 = new JButton("");
	  luchador1.setOpaque(false);
	  luchador1.setEnabled(false);
	  luchador1.setBounds(317, 126, 100, 218);
	  add(luchador1);
	  
	  JButton luchador2 = new JButton("");
	  luchador2.setOpaque(false);
	  luchador2.setEnabled(false);
	  luchador2.setBounds(594, 126, 100, 218);
	  add(luchador2);
	  
	  JLabel imagenLuchador1 = new JLabel("New label");
	  imagenLuchador1.setBounds(317, 126, 100, 218);
	  switch(jugador1.getNombre()) {
	  case "Ikki":
		  imagenLuchador1.setIcon(new ImageIcon("./imagenes/Ikki.jpg"));
	  break;
	  case "Shiryu":
		  imagenLuchador1.setIcon(new ImageIcon("./imagenes/Shiryu.jpg"));
	  break;
	  case "Seiya":
		  imagenLuchador1.setIcon(new ImageIcon("./imagenes/Seiya.jpg"));
	  break;
	  case "Yoga":
		  imagenLuchador1.setIcon(new ImageIcon("./imagenes/Yoga.jpg"));
	  break;
	  case "Shun":
		  imagenLuchador1.setIcon(new ImageIcon("./imagenes/Shun.jpg"));
	  break;	  
	  }
	  add(imagenLuchador1);
	  
	  
  	  
	  
	  
	  /*Con este bot�n volveremos al mapa para poder pasar a otra pantalla*/
	  JButton botonAtras = new JButton("ATR\u00C1S");
	  botonAtras.addMouseListener(new MouseAdapter() {
	  	@Override
	  	public void mouseClicked(MouseEvent e) {
	  		v.irPantallaMapa();
	  	}
	  });
	  botonAtras.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 18));
	  botonAtras.setBounds(47, 59, 136, 25);
	  add(botonAtras);
	  
	  /*Estas dos etiquetas nos sirven para mostrar a los usuarios cu�l es la barra de vida y cu�l la de cosmos*/
	  JLabel etiquetaVida = new JLabel("VIDA");
	  etiquetaVida.setHorizontalAlignment(SwingConstants.CENTER);
	  etiquetaVida.setForeground(Color.WHITE);
	  etiquetaVida.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));
	  etiquetaVida.setBounds(474, 366, 56, 16);
	  add(etiquetaVida);
	  
	  JLabel etiquetaCosmos = new JLabel("COSMOS");
	  etiquetaCosmos.setForeground(Color.WHITE);
	  etiquetaCosmos.setHorizontalAlignment(SwingConstants.CENTER);
	  etiquetaCosmos.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));
	  etiquetaCosmos.setBounds(461, 394, 78, 16);
	  add(etiquetaCosmos);
	  /*Esta etiqueta nos sirve para poner una imagen de fondo a nuestra pantalla de juego*/
	  JLabel etiqueta_fondo_poseidon = new JLabel("");
	  etiqueta_fondo_poseidon.setBounds(0,0,1008,536);
	  etiqueta_fondo_poseidon.setIcon(new ImageIcon("./imagenes/templo_poseidon.jpg"));
	  add(etiqueta_fondo_poseidon);
	  
	 
	  
	  
	  
	 
	  
	 
	  
	 
    }
}
