package interfaces;

import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

import clases.Caballero;
import clases.CaballeroOro;
import clases.Escenario;
import clases.Caballero.ResultadoBatalla;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.awt.Dimension;

public class PantallaSantuario extends JPanel{
	private Ventana ventana;
	private PantallaSantuario thisRef;
	private Caballero jugador2;
	
    public PantallaSantuario (Ventana v) {     
  	  super();
  	  thisRef=this;
  	  setSize(new Dimension(1008, 536));
  	  ventana=v;
  	  setBackground(new Color(153, 153, 255));
  	  setLayout(null);
      
  	  
  	ArrayList<Caballero> caballeroOro=new ArrayList<Caballero>();
  	caballeroOro.add(v.caballeroOroLeo);
  	caballeroOro.add(v.caballeroOroTauro);
  	caballeroOro.add(v.caballeroOroAries);
  	caballeroOro.add(v.caballeroOroEscorpio);
  	caballeroOro.add(v.caballeroOroGeminis);
  	
  	 /*Esta es la primera pantalla de combate de nuestro juego*/
  	 /*Asignamos una variable para mi caballero y el rival*/ 	  
  	  	  Caballero jugador1=v.getJugador().getCaballero(); 
  	  	  jugador2=Caballero.eligeRival(caballeroOro);
  	  	  jugador1.setVidaInicial(120);
  	  	  jugador1.setCosmosInicial(120);
  	  	  
  	  	  
  	  	  
  	
  	
  	  	/*Este panel nos mostrar� el desarrollo de nuestros ataques*/	
  		  JTextPane panelJuego = new JTextPane();
  		  panelJuego.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
  		  panelJuego.setBounds(41, 131, 232, 174);	  
  		  add(panelJuego);
  		  
  		/*Este panel nos mostrar� el desarrollo de los ataques del rival*/  
  		JTextPane panelJuego2 = new JTextPane();
  	    panelJuego2.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
  	    panelJuego2.setBounds(748, 131, 232, 174);
  	    add(panelJuego2);
	
  	  /*Aqu� aparecer�n los puntos obtenidos por cada combate ganado, que se ir�n sumando al total de nuestro usuario*/	
    	  JTextPane textoPuntos = new JTextPane();
    	  textoPuntos.setText("0 ptos");
    	  textoPuntos.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 22));
    	  textoPuntos.setBounds(824, 41, 100, 30);
    	  add(textoPuntos);
    	  
    		/*Esta barra de progreso est� unida a la variable de vida que creamos para nuestro jugador*/	
    	  	JProgressBar vidaJugador = new JProgressBar();
    	  	vidaJugador.setMaximum(120);
    		  vidaJugador.setBackground(Color.WHITE);
    		  vidaJugador.setString("");
    		  vidaJugador.setStringPainted(true);
    		  vidaJugador.setValue(120);
    		  vidaJugador.setToolTipText("");
    		  vidaJugador.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));
    		  vidaJugador.setForeground(new Color(255, 0, 0));
    		  vidaJugador.setBounds(300, 334, 146, 15);   		  
    		  add(vidaJugador);
    		  
    		  /*Esta barra de progreso est� unida a la variable de cosmos que creamos para nuestro jugador*/
    		  JProgressBar cosmosJugador = new JProgressBar();
    		  cosmosJugador.setMaximum(120);
    		  cosmosJugador.setBackground(Color.WHITE);
    		  cosmosJugador.setString("");
    		  cosmosJugador.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));
    		  cosmosJugador.setStringPainted(true);
    		  cosmosJugador.setValue(120);
    		  cosmosJugador.setForeground(new Color(153, 255, 255));
    		  cosmosJugador.setBounds(300, 359, 146, 15);
    		  add(cosmosJugador);
    		  
    		  /*Esta barra ser� la de la vida de nuestro adversario*/
    		  JProgressBar vidaRival = new JProgressBar();
    		  vidaRival.setMaximum(120);
    	 	  vidaRival.setStringPainted(true);
    	 	  vidaRival.setBackground(Color.WHITE);
    	 	  vidaRival.setString("");
    	 	  vidaRival.setValue(120);
    	 	  vidaRival.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));	  	  
    	 	  vidaRival.setForeground(new Color(255, 0, 0));
    	 	  vidaRival.setBounds(563, 334, 146, 15);
    	 	  add(vidaRival);
    		  
    		  /*Esta barra ser� la del cosmos de nuestro adversario*/
    	 	  JProgressBar cosmosRival = new JProgressBar();
    	 	  cosmosRival.setMaximum(120);
    	 	  cosmosRival.setStringPainted(true);
    	 	  cosmosRival.setBackground(Color.WHITE);
    	 	  cosmosRival.setValue(120);
    	 	  cosmosRival.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));
    	 	  cosmosRival.setForeground(new Color(153, 255, 255));
    	 	  cosmosRival.setString("");
    	 	  cosmosRival.setBounds(563, 359, 146, 15);	
    	 	  add(cosmosRival);
    	  
    	 	 JLabel imagenLuchador2 = new JLabel("New label");
    		  imagenLuchador2.setBounds(593, 87, 100, 218);
    		  switch(jugador2.getNombre()) {
    		  case "Aiora":
    			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/leo2.jpg"));
    		  break;
    		  case "Aldebaran":
    			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/tauro2.jpg"));
    		  break;
    		  case "Mu":
    			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/aries2.jpg"));
    		  break;
    		  case "Milo":
    			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/escorpio2.jpg"));
    		  break;
    		  case "Saga":
    			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/geminis2.jpg"));
    		  break;	  
    		  }
    		  imagenLuchador2.setVisible(false);
			  imagenLuchador2.setVisible(true);
    		  add(imagenLuchador2);
    		  
	  /*En este bot�n elegimos el tipo de ataque que menos da�o le har� a nuestro enemigo*/
    	  	  JButton botonGolpe = new JButton("GOLPE");  	  
    	  	  botonGolpe.addMouseListener(new MouseAdapter() {
    	  	  	@Override
    	  	  	public void mouseClicked(MouseEvent arg0) {
    	  	  	ResultadoBatalla rgj=jugador1.ataqueGolpe(jugador2,jugador1);
    	  	  		Caballero.ataqueRival(120, jugador1, jugador2, panelJuego2,vidaJugador,cosmosRival);
    	  	  		   vidaRival.setValue(jugador2.getVida());
    		           cosmosJugador.setValue(jugador1.getCosmos());
    		           panelJuego.setText(jugador1.getNombre()+" invoca "+jugador1.getAtaque()[0].getNombre()+"\nLa vida de "+jugador2.getNombre()+" es "+jugador2.getVida());
    	  	  		switch (rgj) {
    	  	  		case	RIVALMUERTO:
    	  	  		    v.puntosConseguidos=(v.puntosConseguidos+50);
    	  	  		    v.guardarPartida(50);
    	  	  		    panelJuego.setText(jugador1.getNombre()+" ha vencido");
    	  	  			JButton siguienteRival =new JButton("Rival");
    	  	  		    siguienteRival.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 18));
      	  	            
    	  	  		siguienteRival.setBounds(47,10,100,30);
    	  	  			thisRef.add(siguienteRival);
    				     textoPuntos.setText(v.puntosConseguidos+" puntos");
    	  	  			 siguienteRival.addMouseListener(new MouseAdapter() {

    						@Override
    						public void mouseClicked(MouseEvent e) {
    							if(!caballeroOro.isEmpty()) {
    							  jugador2=Caballero.eligeRival(caballeroOro);
    							  switch(jugador2.getNombre()) {
    				    		  case "Aiora":
    				    			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/leo2.jpg"));
    				    		  break;
    				    		  case "Aldebaran":
    				    			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/tauro2.jpg"));
    				    		  break;
    				    		  case "Mu":
    				    			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/aries2.jpg"));
    				    		  break;
    				    		  case "Milo":
    				    			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/escorpio2.jpg"));
    				    		  break;
    				    		  case "Saga":
    				    			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/geminis2.jpg"));
    				    		  break;	  
    				    		  }
    							  imagenLuchador2.setVisible(false);
    							  imagenLuchador2.setVisible(true);
    							  jugador1.setVida(120);
    							  jugador1.setCosmos(120);
    							  
    							  //Actualizar valores a todos los campos de texto relevantes y los progressbar
    							  siguienteRival.setVisible(false);
    							}else {
    								ventana.irPantallaMapa();
    							}
    						}
    	  	  				
    	  	  			});
    	  	  			break;
    	  	  		case CONTINUAR:	
    	  	  			
    	  	  			break;
    	  	  		case ATAQUEFALLIDO:
    	  	  		 panelJuego.setText("El ataque ha fallado");	
    		    	 jugador1.setCosmos(jugador1.getCosmos()-20);
    		    	 cosmosJugador.setValue(jugador1.getCosmos());
    		         cosmosJugador.getValue();
    	  	  		 break;
    	  	  		}
    	  	  	thisRef.setVisible(false);
    	  	  	thisRef.setVisible(true);
    	  	  	}
    	  	  });
    	  	  botonGolpe.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
    	  	  botonGolpe.setBounds(119, 430, 185, 32);
    	  	  add(botonGolpe);
  	 
  	 /*En este bot�n elegimos el tipo de ataque medio*/
  	JButton botonPoder = new JButton("PODER");  	  
	  botonPoder.addMouseListener(new MouseAdapter() {
	  	@Override
	  	public void mouseClicked(MouseEvent arg0) {
	  	ResultadoBatalla rgj=jugador1.ataquePoder(jugador2,jugador1);
	  	   Caballero.ataqueRival(120, jugador1, jugador2, panelJuego2,vidaJugador,cosmosRival);
	  	   vidaRival.setValue(jugador2.getVida());
         cosmosJugador.setValue(jugador1.getCosmos());
         panelJuego.setText(jugador1.getNombre()+" invoca "+jugador1.getAtaque()[1].getNombre()+"\nLa vida de "+jugador2.getNombre()+" es "+jugador2.getVida());
	  		switch (rgj) {
	  		case	RIVALMUERTO:
	  			v.puntosConseguidos=(v.puntosConseguidos+50);
	  			v.guardarPartida(50);
	  			panelJuego.setText(jugador1.getNombre()+" ha vencido");
	  			JButton siguienteRival =new JButton("Rival");
	  			siguienteRival.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 18));
  	  	        
	  			siguienteRival.setBounds(47,10,100,30);
	  			thisRef.add(siguienteRival);
		        textoPuntos.setText(v.puntosConseguidos+" puntos");
	  			siguienteRival.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					if(!caballeroOro.isEmpty()) {
					  jugador2=Caballero.eligeRival(caballeroOro);
					  switch(jugador2.getNombre()) {
		    		  case "Aiora":
		    			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/leo2.jpg"));
		    		  break;
		    		  case "Aldebaran":
		    			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/tauro2.jpg"));
		    		  break;
		    		  case "Mu":
		    			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/aries2.jpg"));
		    		  break;
		    		  case "Milo":
		    			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/escorpio2.jpg"));
		    		  break;
		    		  case "Saga":
		    			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/geminis2.jpg"));
		    		  break;	  
		    		  }
					  imagenLuchador2.setVisible(false);
					  imagenLuchador2.setVisible(true);
					  jugador1.setVida(120);
					  jugador1.setCosmos(120);
					  
					  //Actualizar valores a todos los campos de texto relevantes y los progressbar
					  siguienteRival.setVisible(false);
					}else {
						ventana.irPantallaMapa();
					}
				}
	  				
	  			});
	  			break;
	  		case CONTINUAR:
	  			
	  			break;
	  		case ATAQUEFALLIDO:
	  	 panelJuego.setText("El ataque ha fallado");		
  	 jugador1.setCosmos(jugador1.getCosmos()-30);
  	 cosmosJugador.setValue(jugador1.getCosmos());
       cosmosJugador.getValue();
	  		 break;
	  		}
	  	thisRef.setVisible(false);
	  	thisRef.setVisible(true);
	  	}
	  });
	  botonPoder.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
	  botonPoder.setBounds(398, 430, 185, 32);
	  add(botonPoder);
  	  
  	/*En este bot�n elegiremos el ataque m�s letal que posee nuestro caballero*/	
	  JButton botonFinal = new JButton("FINAL");  	  
  	  botonFinal.addMouseListener(new MouseAdapter() {
  	  	@Override
  	  	public void mouseClicked(MouseEvent e) {
  	  	ResultadoBatalla rgj=jugador1.ataqueFinal(jugador2,jugador1);
	  	   Caballero.ataqueRival(120, jugador1, jugador2, panelJuego2,vidaJugador,cosmosRival);
	  	   vidaRival.setValue(jugador2.getVida());
           cosmosJugador.setValue(jugador1.getCosmos());
           panelJuego.setText(jugador1.getNombre()+" invoca "+jugador1.getAtaque()[2].getNombre()+"\nLa vida de "+jugador2.getNombre()+" es "+jugador2.getVida());
	  		switch (rgj) {
	  		case	RIVALMUERTO:
	  			v.puntosConseguidos=(v.puntosConseguidos+50);
	  	  		v.guardarPartida(50);
	  			panelJuego.setText(jugador1.getNombre()+" ha vencido");
	  			JButton siguienteRival =new JButton("Rival");
	  			siguienteRival.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 18));  	  	        
	  			siguienteRival.setBounds(47,10,100,30);
	  			thisRef.add(siguienteRival);
		        textoPuntos.setText(v.puntosConseguidos+" puntos");
	  			siguienteRival.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					if(!caballeroOro.isEmpty()) {
					  jugador2=Caballero.eligeRival(caballeroOro);	
					  switch(jugador2.getNombre()) {
		    		  case "Aiora":
		    			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/leo2.jpg"));
		    		  break;
		    		  case "Aldebaran":
		    			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/tauro2.jpg"));
		    		  break;
		    		  case "Mu":
		    			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/aries2.jpg"));
		    		  break;
		    		  case "Milo":
		    			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/escorpio2.jpg"));
		    		  break;
		    		  case "Saga":
		    			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/geminis2.jpg"));
		    		  break;	  
		    		  }
		    		  imagenLuchador2.setVisible(false);
					  imagenLuchador2.setVisible(true);
					  jugador1.setVida(120);
					  jugador1.setCosmos(120);
					  
					 
					  //Actualizar valores a todos los campos de texto relevantes y los progressbar
					  siguienteRival.setVisible(false);
					}else {
						ventana.irPantallaMapa();
					}
				}
	  				
	  			});
	  			break;
	  		case CONTINUAR:
	  			
	  			break;
	  		case ATAQUEFALLIDO:
	  	 panelJuego.setText("El ataque ha fallado");
    	 jugador1.setCosmos(jugador1.getCosmos()-50);
    	 cosmosJugador.setValue(jugador1.getCosmos());
         cosmosJugador.getValue();
	  		 break;
	  		}
	  	thisRef.setVisible(false);
	  	thisRef.setVisible(true);
  	  	}
  	  });
  	  botonFinal.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
  	  botonFinal.setBounds(671, 430, 185, 32);
  	  add(botonFinal);
  	  
  
  	  
  	
  	  
  
	  
	  /*Estos dos botones nos sirven para poner las etiquetas de las imagenes de nuestros luchadores*/
	  JButton luchador1 = new JButton("");
	  luchador1.setOpaque(false);
	  luchador1.setEnabled(false);
	  luchador1.setBounds(329, 87, 100, 218);
	  add(luchador1);
	  
	  JButton luchador2 = new JButton("");
	  luchador2.setOpaque(false);
	  luchador2.setEnabled(false);
	  luchador2.setBounds(593, 87, 100, 218);
	  add(luchador2);
	  
	  JLabel imagenLuchador1 = new JLabel("New label");
	  imagenLuchador1.setBounds(329, 87, 100, 218);
	  switch(jugador1.getNombre()) {
	  case "Ikki":
		  imagenLuchador1.setIcon(new ImageIcon("./imagenes/Ikki.jpg"));
	  break;
	  case "Shiryu":
		  imagenLuchador1.setIcon(new ImageIcon("./imagenes/Shiryu.jpg"));
	  break;
	  case "Seiya":
		  imagenLuchador1.setIcon(new ImageIcon("./imagenes/Seiya.jpg"));
	  break;
	  case "Yoga":
		  imagenLuchador1.setIcon(new ImageIcon("./imagenes/Yoga.jpg"));
	  break;
	  case "Shun":
		  imagenLuchador1.setIcon(new ImageIcon("./imagenes/Shun.jpg"));
	  break;	  
	  }
	  add(imagenLuchador1);
	  
	 
  	  
	  
	  
	  /*Con este bot�n volveremos al mapa para poder pasar a otra pantalla*/
	  JButton botonAtras = new JButton("ATRAS");
	  botonAtras.addMouseListener(new MouseAdapter() {
	  	@Override
	  	public void mouseClicked(MouseEvent e) {
	  		v.irPantallaMapa();
	  	}
	  });
	  botonAtras.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 18));
	  botonAtras.setBounds(47, 59, 136, 25);
	  add(botonAtras);
	  
	  /*Estas dos etiquetas nos sirven para mostrar a los usuarios cu�l es la barra de vida y cu�l la de cosmos*/
	  JLabel etiquetaVida = new JLabel("VIDA");
	  etiquetaVida.setHorizontalAlignment(SwingConstants.CENTER);
	  etiquetaVida.setForeground(Color.WHITE);
	  etiquetaVida.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));
	  etiquetaVida.setBounds(486, 331, 56, 16);
	  add(etiquetaVida);
	  
	  JLabel lblCosmos = new JLabel("COSMOS");
	  lblCosmos.setForeground(Color.WHITE);
	  lblCosmos.setHorizontalAlignment(SwingConstants.CENTER);
	  lblCosmos.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));
	  lblCosmos.setBounds(475, 358, 78, 16);
	  add(lblCosmos);
	  
	  /*Esta etiqueta nos sirve para poner una imagen de fondo a nuestra pantalla de juego*/
	  JLabel etiqueta_fondo_santuario = new JLabel("New label");
	  etiqueta_fondo_santuario.setBounds(0,0,1008,536);
	  etiqueta_fondo_santuario.setIcon(new ImageIcon("./imagenes/santuario.jpg"));
	  add(etiqueta_fondo_santuario);
	  
	  
	  
	
	  
	  
	  
	 
	  
	 
	  
	 
    }
}
