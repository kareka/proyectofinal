package interfaces;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.JProgressBar;
import javax.swing.JEditorPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.event.ChangeListener;
import clases.Caballero;
import clases.Caballero.ResultadoBatalla;
import clases.CaballeroBronce;
import clases.Escenario;

import javax.swing.event.ChangeEvent;
import java.awt.ComponentOrientation;
import javax.swing.SwingConstants;
import java.awt.Dimension;

public class PantallaTorneo extends JPanel{
	private Ventana ventana;
	private PantallaTorneo thisRef;
	private Caballero jugador2;
	
	
    public PantallaTorneo (Ventana v) {
  	  super();
  	  thisRef=this;
  	  setSize(new Dimension(1008, 536));
  	  ventana=v;
  	  setBackground(new Color(153, 153, 255));
  	  setLayout(null); 
  	  
  
 	 ArrayList<Caballero> caballeroBronce=new ArrayList<Caballero>();
  	  caballeroBronce.add(v.caballeroBronceDragon);
  	  caballeroBronce.add(v.caballeroBroncePegaso);
  	  caballeroBronce.add(v.caballeroBronceCisne);
  	  caballeroBronce.add(v.caballeroBronceFenix);
  	  caballeroBronce.add(v.caballeroBronceAndromeda);
  	  //Te quita el caballero si est�, si no, no hace nada
  	  caballeroBronce.remove(ventana.getJugador().getCaballero());
  	  
  
  Caballero jugador1=v.getJugador().getCaballero();
  jugador2=Caballero.eligeRival(caballeroBronce);
	  jugador1.setVida(100);
	  jugador1.setCosmos(100);
	  
  
  /*Estos paneles nos mostrar�n el desarrollo de la lucha ataque a ataque*/	  
  JTextPane panelJuego = new JTextPane();
  panelJuego.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 22));
  panelJuego.setBounds(29, 120, 232, 174);	  
  add(panelJuego);
  
  JTextPane panelJuego2 = new JTextPane();
  panelJuego2.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 22));
  panelJuego.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 22));
  panelJuego2.setBounds(737, 120, 232, 174);
  add(panelJuego2);
  	
  /*Aqu� aparecer�n los puntos obtenidos por cada combate ganado, que se ir�n sumando al total de nuestro usuario*/	  
	  JTextPane textoPuntos = new JTextPane();
	  textoPuntos.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 22));
	  textoPuntos.setBounds(814, 41, 100, 30);
      textoPuntos.setText(v.puntosConseguidos+" puntos");
      textoPuntos.getText();
	  add(textoPuntos);
	  
	/*Esta barra de progreso est� unida a la variable de vida que creamos para nuestro jugador*/	  
	   	 JProgressBar vidaJugador = new JProgressBar();
	   	vidaJugador.setBackground(Color.WHITE);
	   	vidaJugador.setStringPainted(true);
	   	vidaJugador.setValue(100);
	   	vidaJugador.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));
	   	vidaJugador.setForeground(new Color(255, 0, 0));
	   	vidaJugador.setBounds(288, 332, 146, 15);
	 	add(vidaJugador);
	 	  
   /*Esta barra de progreso est� unida a la variable de cosmos que creamos para nuestro jugador*/	  
	 	  JProgressBar cosmosJugador = new JProgressBar();
	 	  cosmosJugador.setBackground(Color.WHITE);
	 	  cosmosJugador.setStringPainted(true);
	 	  cosmosJugador.setValue(100);
	 	  cosmosJugador.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));
	 	  cosmosJugador.setForeground(new Color(153, 255, 255));
	 	  cosmosJugador.setBounds(288, 359, 146, 15);
	 	  add(cosmosJugador);
	 	  
   /*Esta barra ser� la de la vida de nuestro adversario*/	  
	 	  JProgressBar vidaRival = new JProgressBar();
	 	  vidaRival.setStringPainted(true);
	 	  vidaRival.setBackground(Color.WHITE);
	 	  vidaRival.setValue(100);
	 	  vidaRival.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));	  	  
	 	  vidaRival.setForeground(new Color(255, 0, 0));
	 	  vidaRival.setBounds(563, 334, 146, 15);
	 	  add(vidaRival);
	 	  
  /*Esta barra ser� la del cosmos de nuestro adversario*/	  
	 	  JProgressBar cosmosRival = new JProgressBar();
	 	  cosmosRival.setStringPainted(true);
	 	  cosmosRival.setBackground(Color.WHITE);
	 	  cosmosRival.setValue(100);
	 	  cosmosRival.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));
	 	  cosmosRival.setForeground(new Color(153, 255, 255));
	 	  cosmosRival.setString("");
	 	  cosmosRival.setBounds(563, 359, 146, 15);	
	 	  add(cosmosRival);
	 	  
	 	 JLabel imagenLuchador2 = new JLabel("New label");
		  imagenLuchador2.setBounds(585, 87, 100, 218);
		  switch(jugador2.getNombre()) {
		  case "Ikki":
			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/Ikki2.jpg"));
		  break;
		  case "Shiryu":
			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/Shiryu2.jpg"));
		  break;
		  case "Seiya":
			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/Seiya2.jpg"));
		  break;
		  case "Yoga":
			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/Yoga2.jpg"));
		  break;
		  case "Shun":
			  imagenLuchador2.setIcon(new ImageIcon("./imagenes/Shun2.jpg"));
		  break;	  
		  }
		  imagenLuchador2.setVisible(false);
		  imagenLuchador2.setVisible(true);
		  add(imagenLuchador2);
	  
 /*En este bot�n elegimos el tipo de ataque que menos da�o le har� a nuestro enemigo*/ 	  
  	  JButton botonGolpe = new JButton("GOLPE");  	  
  	  botonGolpe.addMouseListener(new MouseAdapter() {
  	  	@Override
  	  	public void mouseClicked(MouseEvent arg0) {
  	  	ResultadoBatalla rgj=jugador1.ataqueGolpe(jugador2,jugador1);
  	  		Caballero.ataqueRival(100, jugador1, jugador2, panelJuego2,vidaJugador,cosmosRival);
  	  		   vidaRival.setValue(jugador2.getVida());
	           cosmosJugador.setValue(jugador1.getCosmos());
	           panelJuego.setText(jugador1.getNombre()+" invoca "+jugador1.getAtaque()[0].getNombre()+"\nLa vida de "+jugador2.getNombre()+" es "+jugador2.getVida());
  	  		switch (rgj) {
  	  		case	RIVALMUERTO:
  	  		    v.puntosConseguidos=(v.puntosConseguidos+50);
  	  		    v.guardarPartida(50);
  	  		    panelJuego.setText(jugador1.getNombre()+" ha vencido");
  	  			JButton siguienteRival =new JButton("Rival");
  	  		    siguienteRival.setBounds(47,10,100,30);
  	  		    siguienteRival.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 18));
  	  	        
  	  			thisRef.add(siguienteRival);
			     textoPuntos.setText(v.puntosConseguidos+" puntos");
			      jugador1.setVida(100);
				  jugador1.setCosmos(100);
				  
  	  			 siguienteRival.addMouseListener(new MouseAdapter() {

					@Override
					public void mouseClicked(MouseEvent e) {
						if(!caballeroBronce.isEmpty()) {
						  caballeroBronce.remove(ventana.getJugador().getCaballero());
						  jugador2=Caballero.eligeRival(caballeroBronce);
						  switch(jugador2.getNombre()) {
						  case "Ikki":
							  imagenLuchador2.setIcon(new ImageIcon("./imagenes/Ikki2.jpg"));
						  break;
						  case "Shiryu":
							  imagenLuchador2.setIcon(new ImageIcon("./imagenes/Shiryu2.jpg"));
						  break;
						  case "Seiya":
							  imagenLuchador2.setIcon(new ImageIcon("./imagenes/Seiya2.jpg"));
						  break;
						  case "Yoga":
							  imagenLuchador2.setIcon(new ImageIcon("./imagenes/Yoga2.jpg"));
						  break;
						  case "Shun":
							  imagenLuchador2.setIcon(new ImageIcon("./imagenes/Shun2.jpg"));
						  break;	  
						  }
						  imagenLuchador2.setVisible(false);
						  imagenLuchador2.setVisible(true);
						  
						  //Actualizar valores a todos los campos de texto relevantes y los progressbar
						  siguienteRival.setVisible(false);
						  
						}else {
							ventana.irPantallaMapa();
						}
					}
  	  				
  	  			});
  	  			break;
  	  		case CONTINUAR:	
  	  			
  	  			break;
  	  		case ATAQUEFALLIDO:
  	  		 panelJuego.setText("El ataque ha fallado");	
	    	 jugador1.setCosmos(jugador1.getCosmos()-20);
	    	 cosmosJugador.setValue(jugador1.getCosmos());
	         cosmosJugador.getValue();
  	  		 break;
  	  		}
  	  	thisRef.setVisible(false);
  	  	thisRef.setVisible(true);
  	  	}
  	  });
  	  botonGolpe.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
  	  botonGolpe.setBounds(119, 430, 185, 32);
  	  add(botonGolpe);
 
  /*En este bot�n elegimos el tipo de ataque medio*/	  
  	  JButton botonPoder = new JButton("PODER");  	  
  	  botonPoder.addMouseListener(new MouseAdapter() {
  	  	@Override
  	  	public void mouseClicked(MouseEvent arg0) {
  	  	ResultadoBatalla rgj=jugador1.ataquePoder(jugador2,jugador1);
	  	   Caballero.ataqueRival(100, jugador1, jugador2, panelJuego2,vidaJugador,cosmosRival);
	  	   vidaRival.setValue(jugador2.getVida());
           cosmosJugador.setValue(jugador1.getCosmos());
           panelJuego.setText(jugador1.getNombre()+" invoca "+jugador1.getAtaque()[1].getNombre()+"\nLa vida de "+jugador2.getNombre()+" es "+jugador2.getVida());
	  		switch (rgj) {
	  		case	RIVALMUERTO:
	  			v.puntosConseguidos=(v.puntosConseguidos+50);
  	  		    v.guardarPartida(50);
	  			panelJuego.setText(jugador1.getNombre()+" ha vencido");
	  			JButton siguienteRival =new JButton("Rival");
	  			siguienteRival.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 18));
  	  	        
	  			siguienteRival.setBounds(47,10,100,30);
	  			thisRef.add(siguienteRival);
		        textoPuntos.setText(v.puntosConseguidos+" puntos");
		          jugador1.setVida(100);
				  jugador1.setCosmos(100);
				  
	  			siguienteRival.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					if(!caballeroBronce.isEmpty()) {
					  jugador2=Caballero.eligeRival(caballeroBronce);
					  switch(jugador2.getNombre()) {
					  case "Ikki":
						  imagenLuchador2.setIcon(new ImageIcon("./imagenes/Ikki2.jpg"));
					  break;
					  case "Shiryu":
						  imagenLuchador2.setIcon(new ImageIcon("./imagenes/Shiryu2.jpg"));
					  break;
					  case "Seiya":
						  imagenLuchador2.setIcon(new ImageIcon("./imagenes/Seiya2.jpg"));
					  break;
					  case "Yoga":
						  imagenLuchador2.setIcon(new ImageIcon("./imagenes/Yoga2.jpg"));
					  break;
					  case "Shun":
						  imagenLuchador2.setIcon(new ImageIcon("./imagenes/Shun2.jpg"));
					  break;	  
					  }
					  imagenLuchador2.setVisible(false);
					  imagenLuchador2.setVisible(true);
					  
					  //Actualizar valores a todos los campos de texto relevantes y los progressbar
					  siguienteRival.setVisible(false);
					}else {
						ventana.irPantallaMapa();
					}
				}
	  				
	  			});
	  			break;
	  		case CONTINUAR:
	  			
	  			break;
	  		case ATAQUEFALLIDO:
	  	 panelJuego.setText("El ataque ha fallado");		
    	 jugador1.setCosmos(jugador1.getCosmos()-30);
    	 cosmosJugador.setValue(jugador1.getCosmos());
         cosmosJugador.getValue();
	  		 break;
	  		}
	  	thisRef.setVisible(false);
	  	thisRef.setVisible(true);
  	  	}
  	  });
  	  botonPoder.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
  	  botonPoder.setBounds(398, 430, 185, 32);
  	  add(botonPoder);
  
  /*En este bot�n elegiremos el ataque m�s letal que posee nuestro caballero*/	  
  	  JButton botonFinal = new JButton("FINAL");  	  
  	  botonFinal.addMouseListener(new MouseAdapter() {
  	  	@Override
  	  	public void mouseClicked(MouseEvent e) {
  	  	ResultadoBatalla rgj=jugador1.ataqueFinal(jugador2,jugador1);
	  	   Caballero.ataqueRival(100, jugador1, jugador2, panelJuego2,vidaJugador,cosmosRival);
	  	   vidaRival.setValue(jugador2.getVida());
           cosmosJugador.setValue(jugador1.getCosmos());
           panelJuego.setText(jugador1.getNombre()+" invoca "+jugador1.getAtaque()[2].getNombre()+"\nLa vida de "+jugador2.getNombre()+" es "+jugador2.getVida());
	  		switch (rgj) {
	  		case	RIVALMUERTO:
	  			v.puntosConseguidos=(v.puntosConseguidos+50);
  	  		    v.guardarPartida(50);
	  			panelJuego.setText(jugador1.getNombre()+" ha vencido");
	  			JButton siguienteRival =new JButton("Rival");
	  			siguienteRival.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 18));
  	  	        
	  			siguienteRival.setBounds(47,10,100,30);
	  			thisRef.add(siguienteRival);
		        textoPuntos.setText(v.puntosConseguidos+" puntos");
		          jugador1.setVida(100);
				  jugador1.setCosmos(100);
				 
	  			siguienteRival.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					if(!caballeroBronce.isEmpty()) {
					  jugador2=Caballero.eligeRival(caballeroBronce);	
					  switch(jugador2.getNombre()) {
					  case "Ikki":
						  imagenLuchador2.setIcon(new ImageIcon("./imagenes/Ikki2.jpg"));
					  break;
					  case "Shiryu":
						  imagenLuchador2.setIcon(new ImageIcon("./imagenes/Shiryu2.jpg"));
					  break;
					  case "Seiya":
						  imagenLuchador2.setIcon(new ImageIcon("./imagenes/Seiya2.jpg"));
					  break;
					  case "Yoga":
						  imagenLuchador2.setIcon(new ImageIcon("./imagenes/Yoga2.jpg"));
					  break;
					  case "Shun":
						  imagenLuchador2.setIcon(new ImageIcon("./imagenes/Shun2.jpg"));
					  break;	  
					  }
					  imagenLuchador2.setVisible(false);
					  imagenLuchador2.setVisible(true);
					  
					  //Actualizar valores a todos los campos de texto relevantes y los progressbar
					  siguienteRival.setVisible(false);
					}else {
						ventana.irPantallaMapa();
					}
				}
	  				
	  			});
	  			break;
	  		case CONTINUAR:
	  			
	  			break;
	  		case ATAQUEFALLIDO:
	  	 panelJuego.setText("El ataque ha fallado");
    	 jugador1.setCosmos(jugador1.getCosmos()-50);
    	 cosmosJugador.setValue(jugador1.getCosmos());
         cosmosJugador.getValue();
	  		 break;
	  		}
	  	thisRef.setVisible(false);
	  	thisRef.setVisible(true);
  	  	}
  	  });
  	  botonFinal.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
  	  botonFinal.setBounds(671, 430, 185, 32);
  	  add(botonFinal);
  	  
  
  
  
  
  /*Estos dos botones nos sirven para poner las etiquetas de las imagenes de nuestros luchadores*/  
	  JButton luchador1 = new JButton("");
	  luchador1.setOpaque(false);
	  luchador1.setEnabled(false);
	  luchador1.setBounds(309, 87, 100, 218);
	  add(luchador1);
	  
	  JButton luchador2 = new JButton("");
	  luchador2.setOpaque(false);
	  luchador2.setEnabled(false);
	  luchador2.setBounds(585, 87, 100, 218);
	  add(luchador2);
	  
	  JLabel imagenLuchador1 = new JLabel("New label");
	  imagenLuchador1.setBounds(309, 87, 100, 218);
	  switch(jugador1.getNombre()) {
	  case "Ikki":
		  imagenLuchador1.setIcon(new ImageIcon("./imagenes/Ikki.jpg"));
	  break;
	  case "Shiryu":
		  imagenLuchador1.setIcon(new ImageIcon("./imagenes/Shiryu.jpg"));
	  break;
	  case "Seiya":
		  imagenLuchador1.setIcon(new ImageIcon("./imagenes/Seiya.jpg"));
	  break;
	  case "Yoga":
		  imagenLuchador1.setIcon(new ImageIcon("./imagenes/Yoga.jpg"));
	  break;
	  case "Shun":
		  imagenLuchador1.setIcon(new ImageIcon("./imagenes/Shun.jpg"));
	  break;	  
	  }
	  
	  add(imagenLuchador1);
	  
	  
	  

	  
  /*Con este bot�n volveremos al mapa para poder pasar a otra pantalla*/	  
	  JButton botonAtras = new JButton("ATR\u00C1S");
	  botonAtras.addMouseListener(new MouseAdapter() {
	  	@Override
	  	public void mouseClicked(MouseEvent e) {
	  		v.irPantallaMapa();
	  	}
	  });
	  botonAtras.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 18));
	  botonAtras.setBounds(47, 59, 136, 25);
	  add(botonAtras);
	  
  /*Estas dos etiquetas nos sirven para mostrar a los usuarios cu�l es la barra de vida y cu�l la de cosmos*/	  
	  JLabel etiquetaVida = new JLabel("VIDA");
	  etiquetaVida.setHorizontalAlignment(SwingConstants.CENTER);
	  etiquetaVida.setForeground(Color.WHITE);
	  etiquetaVida.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));
	  etiquetaVida.setBounds(476, 332, 56, 16);
	  add(etiquetaVida);
	  
	  JLabel lblCosmos = new JLabel("COSMOS");
	  lblCosmos.setForeground(Color.WHITE);
	  lblCosmos.setHorizontalAlignment(SwingConstants.CENTER);
	  lblCosmos.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));
	  lblCosmos.setBounds(465, 357, 78, 16);
	  add(lblCosmos);   	  
	   
 /*Esta etiqueta nos sirve para poner una imagen de fondo a nuestra pantalla de juego*/	  
	  JLabel etiqueta_fondo_torneo = new JLabel("");	  	 
	  etiqueta_fondo_torneo.setFont(new Font("Tahoma", Font.PLAIN, 13));
	  etiqueta_fondo_torneo.setBounds(0,0,1008,536);
	  etiqueta_fondo_torneo.setIcon(new ImageIcon("./imagenes/torneo.jpg"));
	  add(etiqueta_fondo_torneo);	 
	    
	  	 	  	 	  	  	  	 
    }
}
