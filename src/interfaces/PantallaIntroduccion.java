package interfaces;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JTextPane;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Dimension;

public class PantallaIntroduccion extends JPanel{
	private Ventana ventana;
	public PantallaIntroduccion(Ventana v) {
		super();
		setSize(new Dimension(1008, 536));
		ventana=v;
    	setBackground(new Color(153, 153, 255));
    	setLayout(null);
    	
    	JLabel lblHistoria = new JLabel("HISTORIA");
    	lblHistoria.setForeground(Color.WHITE);
    	lblHistoria.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 34));
    	lblHistoria.setBounds(706, 82, 184, 36);
    	add(lblHistoria);
    	
    /*Este evento nos hace avanzar hasta la pantalla del mapa de juego*/	
    	JButton botonAvanzar = new JButton("AVANZAR");
    	botonAvanzar.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent arg0) {
    			v.irPantallaMapa();
    		}
    	});
    	botonAvanzar.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
    	botonAvanzar.setBounds(670, 416, 220, 36);
    	add(botonAvanzar);
    	
    /*En este campo de texto hemos a�adido unas propiedades para que aparezca transparente y solo se puedan	
     ver las letras de la historia de cada personaje*/
    	JTextPane campoTexto = new JTextPane();
    	campoTexto.setForeground(Color.WHITE);
    	campoTexto.setOpaque(false);
    	campoTexto.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
    	campoTexto.setBounds(113, 123, 794, 278);
    	campoTexto.setText(v.getJugador().getCaballero().getHistoria());
    	campoTexto.getText();
    	add(campoTexto);
    	
    /*Esta etiqueta nos proporciona un fondo con una imagen para nuestra pantalla de introducci�n*/	
    	JLabel etiquetaFondo = new JLabel("");
    	etiquetaFondo.setBounds(0, 0,1008,536);
    	etiquetaFondo.setIcon(new ImageIcon("./imagenes/fondo_historia.jpg"));
    	add(etiquetaFondo);
    	
	}
}
