package interfaces;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;

public class PantallaFinalShiryu  extends JPanel{
	private Ventana ventana;
	
    public  PantallaFinalShiryu(Ventana v) {
  	  super();
  	  setSize(new Dimension(1008, 536));
  	  ventana=v;
  	  setBackground(new Color(153, 153, 255));
  	  setLayout(null); 
  	  
  	  JTextPane panelShiryu = new JTextPane();
	  panelShiryu.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
	  panelShiryu.setBounds(45, 176, 500, 278);
	  panelShiryu.setForeground(Color.WHITE);
  	  panelShiryu.setOpaque(false);
  	  panelShiryu.setText("Herido de muerte en su �ltimo combate Shiryu viaja al inframundo y "
  	  		+ "all� consigue reencontrarse con su amada Shunrei."
  	  		+ "Como premio por sus servicios Athenea consigue devolverlos a la vida.");
	  add(panelShiryu);
	  
	  JButton btnNewButton = new JButton("SALIR");
	  btnNewButton.addMouseListener(new MouseAdapter() {
	  	@Override
	  	public void mouseClicked(MouseEvent arg0) {
	  		System.exit(0);
	  	}
	  });
	  btnNewButton.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
	  btnNewButton.setBounds(860, 441, 97, 25);
	  add(btnNewButton);
    
    
	  JLabel etiquetaFondo = new JLabel("");
	  etiquetaFondo.setBounds(0, 0, 1008, 536);
	  etiquetaFondo.setIcon(new ImageIcon("./imagenes/finalShiryu.jpg"));
	  add(etiquetaFondo);
    
    }
}

