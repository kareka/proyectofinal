package interfaces;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PantallaFinalIkki  extends JPanel{
	private Ventana ventana;
	
    public  PantallaFinalIkki(Ventana v) {
  	  super();
  	  setSize(new Dimension(1008, 536));
  	  ventana=v;
  	  setBackground(new Color(153, 153, 255));
  	  setLayout(null); 
    
  	  JTextPane panelIkki = new JTextPane();
	  panelIkki.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
	  panelIkki.setBounds(496, 13, 500, 278);
	  panelIkki.setForeground(Color.WHITE);
  	  panelIkki.setOpaque(false);
  	  panelIkki.setText("Ikki ha conseguido lo que persegu�a, convertirse "
  	  		+ "en uno de los caballeros m�s poderosos del universo, gracias "
  	  		+ "a el nombramiento, por parte de Athenea, como Caballero de Cancer.");
	  add(panelIkki);
	  
	  JButton btnNewButton = new JButton("SALIR");
	  btnNewButton.addMouseListener(new MouseAdapter() {
	  	@Override
	  	public void mouseClicked(MouseEvent arg0) {
	  		System.exit(0);
	  	}
	  });
	  btnNewButton.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
	  btnNewButton.setBounds(860, 441, 97, 25);
	  add(btnNewButton);
	  
	  JLabel etiquetaFondo = new JLabel("");
	  etiquetaFondo.setBounds(0, 0, 1008, 536);
	  etiquetaFondo.setIcon(new ImageIcon("./imagenes/finalIkki.jpg"));
	  add(etiquetaFondo);
	  
    }
}

