package interfaces;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;

import clases.Jugador;
import excepciones.EmailIncorrectoException;

import javax.swing.JPasswordField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.awt.Dimension;

public class PantallaRegistro extends JPanel{
	private Ventana ventana;
	private JTextField campoUsuario;
	private JPasswordField campoContrasena;
	private JTextField campoEmail;
    public PantallaRegistro (Ventana v) {
  	  super();
  	  setSize(new Dimension(1008, 536));
  	  ventana=v;
  	  setBackground(new Color(153, 153, 255));
  	  setLayout(null);
  	  
  	  
  	  JLabel etiquetaRegistroDeUsuario = new JLabel("REGISTRO DE USUARIO");
  	  etiquetaRegistroDeUsuario.setForeground(Color.WHITE);
  	  etiquetaRegistroDeUsuario.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 34));
  	  etiquetaRegistroDeUsuario.setBounds(46, 27, 449, 45);
  	  add(etiquetaRegistroDeUsuario);
  	  
  	  campoUsuario = new JTextField();
  	  campoUsuario.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
  	  campoUsuario.setBounds(239, 146, 304, 32);
  	  add(campoUsuario);
  	  campoUsuario.setColumns(10);
  	  
  	  campoContrasena = new JPasswordField();
  	  campoContrasena.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
  	  campoContrasena.setBounds(239, 220, 304, 32);
  	  add(campoContrasena);
  	  
  	  JLabel etiquetaUsuario = new JLabel("USUARIO");
  	  etiquetaUsuario.setForeground(Color.WHITE);
  	  etiquetaUsuario.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
  	  etiquetaUsuario.setBounds(82, 142, 145, 32);
  	  add(etiquetaUsuario);
  	  
  	  JLabel etiquetaPassword = new JLabel("CONTRASE\u00D1A");
  	  etiquetaPassword.setForeground(Color.WHITE);
  	  etiquetaPassword.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
  	  etiquetaPassword.setBounds(12, 220, 214, 32);
  	  add(etiquetaPassword);
  	  
  	  campoEmail = new JTextField();
  	  campoEmail.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
  	  campoEmail.setBounds(239, 311, 304, 32);
  	  add(campoEmail);
  	  campoEmail.setColumns(10);
  	  
  	  JLabel etiquetaEmail = new JLabel("EMAIL");
  	  etiquetaEmail.setForeground(Color.WHITE);
  	  etiquetaEmail.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
  	  etiquetaEmail.setBounds(127, 307, 100, 32);
  	  add(etiquetaEmail);
  	  
  	  JButton botonAtras = new JButton("ATR\u00C1S");
  	  botonAtras.addMouseListener(new MouseAdapter() {
  	  	@Override
  	  	public void mouseClicked(MouseEvent e) {
  	  		v.irPantallaInicio();
  	  	}
  	  });
  	  botonAtras.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
  	  botonAtras.setBounds(50, 410, 207, 32);
  	  add(botonAtras);
  	  
  
  	  
  	  JButton botonRegistrarse = new JButton("REGISTRARSE");
  	  botonRegistrarse.addMouseListener(new MouseAdapter() {
  	  	@Override
  	  	public void mouseClicked(MouseEvent e) {
  	  	Statement rs=null;
  	  	String usuario=campoUsuario.getText();
		String contrasena=String.copyValueOf(
				campoContrasena.getPassword());
		String email=campoEmail.getText();
		
		try {
		rs=v.getConexion().createStatement();         
        rs.executeUpdate("insert into cabzodiaco.jugador(usuario,contrasena,email,puntosAcumulados,caballero_nombre)"
        + " values('"+usuario+"','"+contrasena+"','"+email+"',0,null)"); 
        
			v.setJugador(new Jugador(usuario,contrasena,email));
			
			v.irPantallaModoJuego();
			
		} catch (EmailIncorrectoException e1) {
			
			JOptionPane.showMessageDialog(ventana, "El email es incorrecto, no contiene '@'.","Error en email",JOptionPane.ERROR_MESSAGE);			
			//e1.printStackTrace();
			//v.irPantallaInicio();
		} catch(SQLIntegrityConstraintViolationException ex) {
			JOptionPane.showMessageDialog(ventana, "El usuario ya existe","Usuario ya existente",JOptionPane.ERROR_MESSAGE);			
		}catch (SQLException e1) {
		
			e1.printStackTrace();
		}}
		
  	  });
  	  botonRegistrarse.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
  	  botonRegistrarse.setBounds(336, 410, 207, 32);
  	  add(botonRegistrarse);
  	  
  	  JLabel etiquetaFondoRegistro = new JLabel("");
  	  etiquetaFondoRegistro.setBounds(0, 0, 1008, 536);
  	  etiquetaFondoRegistro.setIcon(new ImageIcon("./imagenes/fondo_registro.jpg"));
  	  add(etiquetaFondoRegistro);
    }
}
