package interfaces;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JTextPane;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;


public class PantallaFinalYoga extends JPanel{
	private Ventana ventana;
	
    public  PantallaFinalYoga(Ventana v) {
  	  super();
  	  setSize(new Dimension(1008, 536));
  	  ventana=v;
  	  setBackground(new Color(153, 153, 255));
  	  setLayout(null);
  	  
  	  JTextPane panelYoga = new JTextPane();
  	  panelYoga.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
  	  panelYoga.setBounds(496, 23, 500, 278);
  	  panelYoga.setForeground(Color.WHITE);
  	  panelYoga.setOpaque(false);
  	  panelYoga.setText("Con la ayuda de Athenea y los Caballeros de Bronce, "
  	  		+ "Yoga consigue recuperar el cuerpo de su madre, de esta forma, "
  	  		+ "sus restos yacen junto a los de su padre en el pante�n familiar.");
  	  add(panelYoga);
  	  
  	  JButton btnNewButton = new JButton("SALIR");
	  btnNewButton.addMouseListener(new MouseAdapter() {
	  	@Override
	  	public void mouseClicked(MouseEvent arg0) {
	  		System.exit(0);
	  	}
	  });
	  btnNewButton.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
	  btnNewButton.setBounds(860, 441, 97, 25);
	  add(btnNewButton);
    
    
  	  JLabel etiquetaFondo = new JLabel("");
  	  etiquetaFondo.setBounds(0, 0, 1008, 536);
  	  etiquetaFondo.setIcon(new ImageIcon("./imagenes/finalYoga.jpg"));
  	  add(etiquetaFondo);
    }
}
