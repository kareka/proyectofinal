package interfaces;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.SwingConstants;

import clases.Ataque;
import clases.CaballeroBronce;
import clases.Jugador;
import excepciones.EmailIncorrectoException;
import java.awt.Dimension;

public class PantallaLogin extends JPanel{
    private Ventana ventana;
    private JTextField campoUsuario;
    private JPasswordField campoContrasena;
    public PantallaLogin (Ventana v) {
    	super();
    	setSize(new Dimension(1008, 536));
    	ventana=v;
    	setBackground(new Color(153, 153, 255));
    	setLayout(null);
    	
    	campoUsuario = new JTextField();
    	campoUsuario.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
    	campoUsuario.setHorizontalAlignment(SwingConstants.CENTER);
    	campoUsuario.setToolTipText("");
    	campoUsuario.setBounds(101, 432, 322, 32);  
    	add(campoUsuario);
    	campoUsuario.setColumns(10);
    	
    	campoContrasena = new JPasswordField();
    	campoContrasena.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
    	campoContrasena.setBounds(586, 432, 322, 32);    	
    	add(campoContrasena);
    /*Evento por el cual nos volvemos a la pantalla de inicio*/	
    	JButton botonAtras = new JButton("ATR\u00C1S");
    	botonAtras.addMouseListener(new MouseAdapter() {
    		@Override
    		public void mouseClicked(MouseEvent e) {
    			v.irPantallaInicio();
    		}
    	});
    	botonAtras.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 22));
    	botonAtras.setBounds(34, 39, 159, 32);
    	add(botonAtras);
    	
    	
    /*En este evento hacemos login, comparando nuestros datos de los campos usuario y 
     contrasena con los que tenemos en base de datos, si son correcto se loguear� el usuario y
     nos mover� a la pantalla mapa, para continuar nuestra ultima partida.
     Si no es correcto volveremos a la pantalla de registro*/	
    	JButton botonLogin = new JButton("LOGIN");
    	botonLogin.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e) {
    			String usuario=campoUsuario.getText();
				String contrasena=String.copyValueOf(
						campoContrasena.getPassword());
				 String[] queryValues={usuario,contrasena};
		            PreparedStatement ls;
					try {
						ls = v.getConexion().prepareStatement(
						        "select * from jugador where usuario=? and contrasena=? ");
						   ls.setString(1,usuario);
				            ls.setString(2,contrasena);
				         //   System.out.println(usuario+" : "+contrasena);
				            ResultSet usuarioEncontrado=ls.executeQuery();
				            if(usuarioEncontrado.next()){
				            	//Saco los datos de la bbdd de usuario para crear jugador
				            	String usuario1= usuarioEncontrado.getString("usuario");
				            	String contrasena1=usuarioEncontrado.getString("contrasena");
				            	String email1=usuarioEncontrado.getString("email");
				            	int puntosAcumulados1=usuarioEncontrado.getInt("puntosAcumulados");
				            	String caballero_nombre1=usuarioEncontrado.getString("caballero_nombre");
				            		
				            	System.out.println("Se ha logueado correctamente");
				                //Saco los datos de caballero para poder insertarlos en jugador
				                ls = v.getConexion().prepareStatement(
				                		"select * from caballerobronce cb,caballero c where cb.caballero_nombre=? and cb.caballero_nombre=c.nombre");
				                ls.setString(1, caballero_nombre1);
				                ResultSet miCaballero=ls.executeQuery();
				                if(miCaballero.next()) {
				                	String constelacion1=miCaballero.getString("cb.constelacion");
				                	String historia1=miCaballero.getString("cb.historia");
				                	String nombre1=miCaballero.getString("cb.caballero_nombre");
				                	String armadura1=miCaballero.getString("c.armadura");
				                	String procedencia1=miCaballero.getString("c.procedencia");
				                	int vida1=miCaballero.getInt("c.vida");
				                	int cosmos1=miCaballero.getInt("c.cosmos");
				                	//Saco los datos de la tabla ataque
				                	ls = v.getConexion().prepareStatement(
					                		"select * from ataque where caballero_nombre=?");
					                ls.setString(1,nombre1);
					                ResultSet miAtaque=ls.executeQuery();
					                Ataque[] miArray= new Ataque[3];
					                int i=0;
					                while(miAtaque.next()) {
					                	String nombreAtaque1=miAtaque.getString("nombre");
					                	String tipo1=miAtaque.getString("tipo");
					                	int nivelDano1=miAtaque.getInt("nivelDano");
					                	int gastoCosmos1=miAtaque.getInt("gastoCosmos");
	
					                Ataque miAtaque1= new Ataque(nombreAtaque1,tipo1,nivelDano1,gastoCosmos1);	
					                miArray[i]=miAtaque1;
					                i++;
					                }
				                CaballeroBronce caballero1=new CaballeroBronce(constelacion1,historia1,nombre1,
				                		armadura1,procedencia1,vida1,cosmos1,miArray);	
				                try {
									ventana.setJugador(new Jugador(usuario1,contrasena1,email1,puntosAcumulados1,caballero1));
								
				                } catch (EmailIncorrectoException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}   
				                v.irPantallaMapa();
				                }
				              
				            }else {
				            	System.out.println("No se ha podido loguear");
				            	v.irPantallaInicio();
				            }
				           
				            
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						
					}
		         
    			
    		}
    	});
    	botonLogin.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
    	botonLogin.setBounds(761, 42, 194, 32);
    	add(botonLogin);
    	
    	  JLabel etiquetaUsuario = new JLabel("USUARIO");
    	  etiquetaUsuario.setHorizontalAlignment(SwingConstants.CENTER);
    	  etiquetaUsuario.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
    	  etiquetaUsuario.setBounds(178, 393, 184, 25);
    	  add(etiquetaUsuario);
    	  
    	  JLabel etiquetaContrasena = new JLabel("CONTRASE\u00D1A");
    	  etiquetaContrasena.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
    	  etiquetaContrasena.setBounds(652, 393, 184, 25);
    	  add(etiquetaContrasena);
    	  
    	  
    /*Esta etiqueta nos permite poner un fondo de pantalla con una imagen*/	
    	JLabel etiquetaFondoLogin = new JLabel("");
    	etiquetaFondoLogin.setBounds(0, 0, 1008, 536);
    	etiquetaFondoLogin.setIcon(new ImageIcon("./imagenes/fondo_login.jpg"));
    	add(etiquetaFondoLogin);
    	  

    	ventana.setSize(this.getWidth(),this.getHeight());
    }
}
