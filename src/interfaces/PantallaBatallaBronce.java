package interfaces;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTextPane;

import clases.Caballero;

import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JProgressBar;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PantallaBatallaBronce extends JPanel{
	private Ventana ventana;
	public PantallaBatallaBronce (Ventana v) {
		super();
		setSize(new Dimension(1008, 536));
		ventana=v;
		setBackground(new Color(153, 153, 255));
		setLayout(null);
		
		ArrayList<Caballero> athenea =new ArrayList<Caballero>();
		athenea.add(v.caballeroBronceDragon);
		athenea.add(v.caballeroBroncePegaso);
		athenea.add(v.caballeroBronceCisne);
		athenea.add(v.caballeroBronceFenix);
		athenea.add(v.caballeroBronceAndromeda);
		
		ArrayList<Caballero> patriarca =new ArrayList<Caballero>();
		patriarca.add(v.caballeroOroLeo);
		patriarca.add(v.caballeroOroTauro);
		patriarca.add(v.caballeroOroAries);
		patriarca.add(v.caballeroOroEscorpio);
		patriarca.add(v.caballeroOroGeminis);
		
		ArrayList<Caballero> hilda =new ArrayList<Caballero>();
		hilda.add(v.caballeroHildaZeta);
		hilda.add(v.caballeroHildaEpsilon);
		hilda.add(v.caballeroHildaBeta);
		hilda.add(v.caballeroHildaGamma);
		hilda.add(v.caballeroHildaAlpha);
		
		ArrayList<Caballero> poseidon=new ArrayList<Caballero>();
		poseidon.add(v.caballeroPoseidonKraken);
		poseidon.add(v.caballeroPoseidonSirena);
		poseidon.add(v.caballeroPoseidonChrysaor);
		poseidon.add(v.caballeroPoseidonCaballoMarino);
		poseidon.add(v.caballeroPoseidonDragonDeLosMares);
		
		
		Caballero jugador1=athenea.get(0);
		Caballero jugador2=athenea.get(1);
		Caballero jugador3=athenea.get(2);
		Caballero jugador4=athenea.get(3);
		Caballero jugador5=athenea.get(4);
		
		Caballero rival1=patriarca.get(0);
		Caballero rival2=patriarca.get(1);
		Caballero rival3=patriarca.get(2);
		Caballero rival4=patriarca.get(3);
		Caballero rival5=patriarca.get(4);
		
		
		JButton botonJ1 = new JButton("");
		botonJ1.setBounds(60, 67, 80, 120);
		botonJ1.setOpaque(false);
		botonJ1.setEnabled(false);
		add(botonJ1);
		
		JButton botonJ2 = new JButton("");
		botonJ2.setBounds(60, 263, 80, 120);
		botonJ2.setOpaque(false);
		botonJ2.setEnabled(false);
		add(botonJ2);
		
		JButton botonJ3 = new JButton("");
		botonJ3.setBounds(187, 67, 80, 120);
		botonJ3.setOpaque(false);
		botonJ3.setEnabled(false);
		add(botonJ3);
		
		JButton botonJ4 = new JButton("");
		botonJ4.setBounds(187, 263, 80, 120);
		botonJ4.setOpaque(false);
		botonJ4.setEnabled(false);
		add(botonJ4);
		
		JButton botonJ5 = new JButton("");
		botonJ5.setBounds(310, 67, 80, 120);
		botonJ5.setOpaque(false);
		botonJ5.setEnabled(false);
		add(botonJ5);
		
		JButton botonR1 = new JButton("");
		botonR1.setBounds(820, 67, 80, 120);
		botonR1.setOpaque(false);
		botonR1.setEnabled(false);
		add(botonR1);
		
		JButton botonR3 = new JButton("");
		botonR3.setBounds(675, 66, 80, 120);
		botonR3.setOpaque(false);
		botonR3.setEnabled(false);
		add(botonR3);
		
		JButton botonR2 = new JButton("");
		botonR2.setBounds(820, 263, 80, 120);
		botonR2.setOpaque(false);
		botonR2.setEnabled(false);
		add(botonR2);
		
		JButton botonR4 = new JButton("");
		botonR4.setBounds(675, 263, 80, 120);
		botonR4.setOpaque(false);
		botonR4.setEnabled(false);
		add(botonR4);
		
		JButton botonR5 = new JButton("");
		botonR5.setBounds(542, 67, 80, 120);
		botonR5.setOpaque(false);
		botonR5.setEnabled(false);
		add(botonR5);
		
		JTextPane panelJuego = new JTextPane();
		panelJuego.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
		panelJuego.setBounds(291, 245, 359, 192);
		add(panelJuego);
		
		JButton btnGolpe = new JButton("GOLPE");
		btnGolpe.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
		btnGolpe.setBounds(98, 465, 160, 40);
		add(btnGolpe);
		
		JButton btnPoder = new JButton("PODER");
		btnPoder.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
		btnPoder.setBounds(398, 465, 160, 40);
		add(btnPoder);
		
		JButton btnFinal = new JButton("FINAL");
		btnFinal.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
		btnFinal.setBounds(708, 465, 160, 40);
		add(btnFinal);
		
		JProgressBar vidaJ1 = new JProgressBar();
		vidaJ1.setForeground(Color.RED);
		vidaJ1.setValue(100);
		vidaJ1.setStringPainted(true);
		vidaJ1.setBounds(60, 201, 80, 14);
		add(vidaJ1);
		
		JProgressBar cosmosJ1 = new JProgressBar();
		cosmosJ1.setForeground(Color.CYAN);
		cosmosJ1.setValue(100);
		cosmosJ1.setStringPainted(true);
		cosmosJ1.setBounds(60, 228, 80, 14);
		add(cosmosJ1);
		
		JProgressBar vidaJ2 = new JProgressBar();
		vidaJ2.setValue(100);
		vidaJ2.setForeground(Color.RED);
		vidaJ2.setToolTipText("");
		vidaJ2.setStringPainted(true);
		vidaJ2.setBounds(60, 396, 80, 14);
		add(vidaJ2);
		
		JProgressBar cosmosJ2 = new JProgressBar();
		cosmosJ2.setForeground(Color.CYAN);
		cosmosJ2.setValue(100);
		cosmosJ2.setStringPainted(true);
		cosmosJ2.setBounds(60, 423, 80, 14);
		add(cosmosJ2);
		
		JProgressBar vidaJ3 = new JProgressBar();
		vidaJ3.setForeground(Color.RED);
		vidaJ3.setValue(100);
		vidaJ3.setStringPainted(true);
		vidaJ3.setBounds(187, 200, 80, 14);
		add(vidaJ3);
		
		JProgressBar cosmosJ3 = new JProgressBar();
		cosmosJ3.setForeground(Color.CYAN);
		cosmosJ3.setValue(100);
		cosmosJ3.setStringPainted(true);
		cosmosJ3.setBounds(187, 228, 80, 14);
		add(cosmosJ3);
		
		JProgressBar vidaJ4 = new JProgressBar();
		vidaJ4.setForeground(Color.RED);
		vidaJ4.setValue(100);
		vidaJ4.setStringPainted(true);
		vidaJ4.setBounds(187, 396, 80, 14);
		add(vidaJ4);
		
		JProgressBar cosmosJ4 = new JProgressBar();
		cosmosJ4.setForeground(Color.CYAN);
		cosmosJ4.setValue(100);
		cosmosJ4.setStringPainted(true);
		cosmosJ4.setBounds(187, 423, 80, 14);
		add(cosmosJ4);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setValue(100);
		progressBar.setStringPainted(true);
		progressBar.setForeground(Color.RED);
		progressBar.setBounds(310, 200, 80, 14);
		add(progressBar);
		
		JProgressBar progressBar_1 = new JProgressBar();
		progressBar_1.setValue(100);
		progressBar_1.setStringPainted(true);
		progressBar_1.setForeground(Color.CYAN);
		progressBar_1.setBounds(310, 228, 80, 14);
		add(progressBar_1);
		
		JProgressBar vidaR5 = new JProgressBar();
		vidaR5.setValue(100);
		vidaR5.setStringPainted(true);
		vidaR5.setForeground(Color.RED);
		vidaR5.setBounds(542, 200, 80, 14);
		add(vidaR5);
		
		JProgressBar cosmosR5 = new JProgressBar();
		cosmosR5.setValue(100);
		cosmosR5.setStringPainted(true);
		cosmosR5.setForeground(Color.CYAN);
		cosmosR5.setBounds(542, 228, 80, 14);
		add(cosmosR5);
		
		JProgressBar vidaR3 = new JProgressBar();
		vidaR3.setValue(100);
		vidaR3.setForeground(Color.RED);
		vidaR3.setStringPainted(true);
		vidaR3.setBounds(675, 199, 80, 14);
		add(vidaR3);
		
		JProgressBar cosmosR3 = new JProgressBar();
		cosmosR3.setValue(100);
		cosmosR3.setStringPainted(true);
		cosmosR3.setForeground(Color.CYAN);
		cosmosR3.setBounds(675, 228, 80, 14);
		add(cosmosR3);
		
		JProgressBar vidaR1 = new JProgressBar();
		vidaR1.setValue(100);
		vidaR1.setStringPainted(true);
		vidaR1.setForeground(Color.RED);
		vidaR1.setBounds(820, 200, 80, 14);
		add(vidaR1);
		
		JProgressBar cosmosR1 = new JProgressBar();
		cosmosR1.setValue(100);
		cosmosR1.setStringPainted(true);
		cosmosR1.setForeground(Color.CYAN);
		cosmosR1.setBounds(820, 228, 80, 14);
		add(cosmosR1);
		
		JProgressBar vidaR4 = new JProgressBar();
		vidaR4.setValue(100);
		vidaR4.setStringPainted(true);
		vidaR4.setForeground(Color.RED);
		vidaR4.setBounds(675, 396, 80, 14);
		add(vidaR4);
		
		JProgressBar cosmosR4 = new JProgressBar();
		cosmosR4.setValue(100);
		cosmosR4.setStringPainted(true);
		cosmosR4.setForeground(Color.CYAN);
		cosmosR4.setBounds(675, 423, 80, 14);
		add(cosmosR4);
		
		JProgressBar vidaR2 = new JProgressBar();
		vidaR2.setValue(100);
		vidaR2.setStringPainted(true);
		vidaR2.setForeground(Color.RED);
		vidaR2.setBounds(820, 396, 80, 14);
		add(vidaR2);
		
		JProgressBar cosmosR2 = new JProgressBar();
		cosmosR2.setValue(100);
		cosmosR2.setStringPainted(true);
		cosmosR2.setForeground(Color.CYAN);
		cosmosR2.setBounds(820, 423, 80, 14);
		add(cosmosR2);
		
		JLabel imagenJ1 = new JLabel("");
		imagenJ1.setBounds(60, 67, 80, 120);
		imagenJ1.setIcon(new ImageIcon("./imagenes/equipos/ShiryuIz.jpg"));
		add(imagenJ1);
		
		JLabel imagenJ3 = new JLabel("");
		imagenJ3.setBounds(187, 67, 80, 120);
		imagenJ3.setIcon(new ImageIcon("./imagenes/equipos/yogaIz.jpg"));
		add(imagenJ3);
		
		JLabel imagenJ5 = new JLabel("");
		imagenJ5.setBounds(310, 67, 80, 120);
		imagenJ5.setIcon(new ImageIcon("./imagenes/equipos/ShunIz.jpg"));
		add(imagenJ5);
		
		JLabel imagenJ2 = new JLabel("");
		imagenJ2.setBounds(60, 263, 80, 120);
		imagenJ2.setIcon(new ImageIcon("./imagenes/equipos/SeiyaIz.jpg"));
		add(imagenJ2);
		
		JLabel imagenJ4 = new JLabel("");
		imagenJ4.setBounds(187, 263, 80, 120);
		imagenJ4.setIcon(new ImageIcon("./imagenes/equipos/IkkiIz.jpg"));
		add(imagenJ4);
		
		JButton btnAtras = new JButton("ATRAS");
		btnAtras.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				v.irPantallaElegirEquipo();
			}
		});
		btnAtras.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
		btnAtras.setBounds(859, 13, 110, 25);
		add(btnAtras);
		
		JLabel imagenR5 = new JLabel("");
		imagenR5.setBounds(542, 67, 80, 120);			
		imagenR5.setIcon(new ImageIcon("./imagenes/equipos/geminisDe.jpg"));				
		add(imagenR5);
		
		JLabel imagenR3 = new JLabel("");
		imagenR3.setBounds(675, 67, 80, 120);		
		imagenR3.setIcon(new ImageIcon("./imagenes/equipos/ariesDe.jpg"));						
		add(imagenR3);
		
		JLabel imagenR1 = new JLabel("");
		imagenR1.setBounds(820, 67, 80, 119);		
		imagenR1.setIcon(new ImageIcon("./imagenes/equipos/leoDe.jpg"));			
		add(imagenR1);
		
		JLabel imagenR4 = new JLabel("");
		imagenR4.setBounds(675, 263, 80, 120);		
		imagenR4.setIcon(new ImageIcon("./imagenes/equipos/escorpioDe.jpg"));	
		add(imagenR4);
		
		JLabel imagenR2 = new JLabel("");
		imagenR2.setBounds(820, 263, 80, 120);		
		imagenR2.setIcon(new ImageIcon("./imagenes/equipos/tauroDe.jpg"));	
		add(imagenR2);
		
		 /*Esta etiqueta nos sirve para poner una imagen de fondo a nuestra pantalla de juego*/	  
		  JLabel etiqueta_fondo_torneo = new JLabel("");	  	 
		  etiqueta_fondo_torneo.setFont(new Font("Tahoma", Font.PLAIN, 13));
		  etiqueta_fondo_torneo.setBounds(0,0,1008,536);
		  etiqueta_fondo_torneo.setIcon(new ImageIcon("./imagenes/torneo.jpg"));
		  add(etiqueta_fondo_torneo);
	  	
	}
}
