package interfaces;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.LineBorder;

import clases.Jugador;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.Dimension;

public class PantallaElegirPersonaje extends JPanel{
	private Ventana ventana;
	 public PantallaElegirPersonaje (Ventana v) {
   	  super();
   	  setSize(new Dimension(1008, 536));
   	  ventana=v;
   	  setBackground(new Color(153, 153, 255));
   	  setLayout(null);
   	  
   	  JLabel etiquetaEligeUnCaballero = new JLabel("ELIGE UN CABALLERO");
   	  etiquetaEligeUnCaballero.setForeground(Color.WHITE);
   	  etiquetaEligeUnCaballero.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 34));
   	  etiquetaEligeUnCaballero.setBounds(312, 0, 403, 32);
   	  add(etiquetaEligeUnCaballero);
   	  
   	  
   /*Este boton pertenece al caballero Seiya, por medio de une evento lo asignaremos a nuestro	  
    usuario y nos mandar� a la pantalla de introducci�n*/
   	  JButton botonSeiya = new JButton("SEIYA");
   	  botonSeiya.addMouseListener(new MouseAdapter() {
   	  	@Override
   	  	public void mouseClicked(MouseEvent arg0) {   	
   	  	v.asignaCaballero("Seiya");
   	    v.irPantallaIntroduccion();
   	  	}
   	  });
   	  botonSeiya.setContentAreaFilled(false);
   	  botonSeiya.setBorderPainted(false);
   	  botonSeiya.setBorder(null);   	
   	  botonSeiya.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 16));
   	  botonSeiya.setBounds(35, 80, 150, 350);
   	/*Con este c�digo ponemos una imagen a este bot�n*/  
   	  botonSeiya.setIcon(new ImageIcon("./imagenes/Bronce/Seiya4.jpg"));
   	  add(botonSeiya);
   	  
   	  
   	/*Este boton pertenece al caballero Shiryu, por medio de une evento lo asignaremos a nuestro	  
      usuario y nos mandar� a la pantalla de introducci�n*/ 
   	  JButton botonShiryu = new JButton("SHIRYU");   	
   	  botonShiryu.addMouseListener(new MouseAdapter() {
   	  	@Override
   	  	public void mouseClicked(MouseEvent e) {   	  	
   	       v.asignaCaballero("Shiryu");               
   	       v.irPantallaIntroduccion();
   	  	}
   	  });
   	  botonShiryu.setContentAreaFilled(false);
   	  botonShiryu.setBorderPainted(false);
   	  botonShiryu.setBorder(null);
   	  botonShiryu.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 16));
   	  botonShiryu.setBounds(223, 80, 150, 350);
   	/*Con este c�digo ponemos una imagen a este bot�n*/ 
   	  botonShiryu.setIcon(new ImageIcon("./imagenes/Bronce/Shiryu4.jpg"));
   	  add(botonShiryu);
   	  
   	
   	/*Este boton pertenece al caballero Yoga, por medio de une evento lo asignaremos a nuestro	  
      usuario y nos mandar� a la pantalla de introducci�n*/  
   	  JButton botonYoga = new JButton("YOGA");
   	  botonYoga.addMouseListener(new MouseAdapter() {
   	  	@Override
   	  	public void mouseClicked(MouseEvent e) {
   	       v.asignaCaballero("Yoga");
   	       v.irPantallaIntroduccion();
   	  	}
   	  });
   	  botonYoga.setContentAreaFilled(false);
   	  botonYoga.setBorderPainted(false);
   	  botonYoga.setBorder(null);
   	  botonYoga.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 16));
   	  botonYoga.setBounds(411, 80, 150, 350);
   	/*Con este c�digo ponemos una imagen a este bot�n*/ 
   	  botonYoga.setIcon(new ImageIcon("./imagenes/Bronce/Yoga4.jpg"));
   	  add(botonYoga);
   	  
   	/*Este boton pertenece al caballero Shun, por medio de une evento lo asignaremos a nuestro	  
      usuario y nos mandar� a la pantalla de introducci�n*/
   	  JButton botonShun = new JButton("SHUN");
   	  botonShun.addMouseListener(new MouseAdapter() {
   	  	@Override
   	  	public void mouseClicked(MouseEvent e) {
   	  	 v.asignaCaballero("Shun");
   	     v.irPantallaIntroduccion();
   	  	}
   	  });
   	  botonShun.setContentAreaFilled(false);
   	  botonShun.setBorderPainted(false);
   	  botonShun.setBorder(null);
   	  botonShun.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 16));
   	  botonShun.setBounds(600, 80, 150, 350);
   	/*Con este c�digo ponemos una imagen a este bot�n*/ 
   	  botonShun.setIcon(new ImageIcon("./imagenes/Bronce/Andromeda4.jpg"));
   	  add(botonShun);
   	  
   	/*Este boton pertenece al caballero Ikki, por medio de une evento lo asignaremos a nuestro	  
      usuario y nos mandar� a la pantalla de introducci�n*/
   	  JButton botonIkki = new JButton("IKKI");   	 
   	  botonIkki.addMouseListener(new MouseAdapter() {
   	  	@Override
   	  	public void mouseClicked(MouseEvent e) {
   	      v.asignaCaballero("Ikki"); 
   	      v.irPantallaIntroduccion();
   	  	}
   	  });
   	  botonIkki.setContentAreaFilled(false);
   	  botonIkki.setBorderPainted(false);
   	  botonIkki.setBorder(null);
   	  botonIkki.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 16));
   	  botonIkki.setBounds(801, 80, 150, 350);
   	/*Con este c�digo ponemos una imagen a este bot�n*/ 
   	  botonIkki.setIcon(new ImageIcon("./imagenes/Bronce/Fenix4.jpg"));
   	  add(botonIkki);
   	  
   	
  	 /*Esta etiqueta sirve para poner una imagen de fondo para la pantalla elegir personaje*/ 
   	  JLabel fondo_elegir_personaje = new JLabel("");
   	  fondo_elegir_personaje.setBorder(null);
   	  fondo_elegir_personaje.setBounds(0,0,1008,536);
   	  fondo_elegir_personaje.setIcon(new ImageIcon("./imagenes/fondo_elegir_personaje.jpg"));
   	  add(fondo_elegir_personaje);
   	  
   	 
   	  
   	 
	 }
}
