package interfaces;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Dimension;

public class PantallaModoJuego extends JPanel{
	private Ventana ventana;
	 public PantallaModoJuego (Ventana v) {
   	  super();
   	  setSize(new Dimension(1008, 536));
   	  ventana=v;
   	  setBackground(new Color(153, 153, 255));
   	  setLayout(null);
   	  
   	  JLabel etiquetaModosDeJuego = new JLabel("MODOS DE JUEGO");
   	  etiquetaModosDeJuego.setForeground(Color.BLACK);
   	  etiquetaModosDeJuego.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 34));
   	  etiquetaModosDeJuego.setBounds(309, 46, 332, 32);
   	  add(etiquetaModosDeJuego);
   	  
   	  JButton botonModoHistoria = new JButton("MODO HISTORIA");
   	  botonModoHistoria.addMouseListener(new MouseAdapter() {
   	  	@Override
   	  	public void mouseClicked(MouseEvent e) {
   	  		v.irPantallaElegirPersonaje();
   	  	}
   	  });
   	  botonModoHistoria.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
   	  botonModoHistoria.setBounds(323, 212, 298, 32);
   	  add(botonModoHistoria);
   	  
   	  JButton botonModoEquipos = new JButton("MODO EQUIPOS");   	 
   	  botonModoEquipos.addMouseListener(new MouseAdapter() {
   	  	@Override
   	  	public void mouseClicked(MouseEvent arg0) {
   	  		v.irPantallaElegirEquipo();
   	  	}
   	  });
   	  botonModoEquipos.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
   	  botonModoEquipos.setBounds(323, 345, 298, 32);
   	  add(botonModoEquipos);
   	  
   	  JLabel etiqueta_fondo_modo_juego = new JLabel("");
   	  etiqueta_fondo_modo_juego.setBounds(0, 0,1008,536);
   	  etiqueta_fondo_modo_juego.setIcon(new ImageIcon("./imagenes/fondo_modo_juego.jpg"));
   	  add(etiqueta_fondo_modo_juego);
	 }
}
