package interfaces;

import javax.swing.JPanel;

import interfaces.Ventana;
import javax.swing.JLabel;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Rectangle;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Dimension;

public class PantallaInicio extends JPanel{
	private Ventana ventana;
      public PantallaInicio (Ventana v) {
          
    	  super();
    	  setSize(new Dimension(1008, 536));
    	  ventana=v;
    	  setBackground(new Color(153, 153, 255));
    	  setLayout(null);
    	  
    	  JLabel etiquetaTitulo = new JLabel("CABALLEROS DEL ZODIACO");
    	  etiquetaTitulo.setForeground(Color.WHITE);
    	  etiquetaTitulo.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 34));
    	  etiquetaTitulo.setBounds(261, 41, 483, 37);
    	  add(etiquetaTitulo);
    	  
    	  JLabel etiquetaVersión = new JLabel("VERSI\u00D3N 1.0");
    	  etiquetaVersión.setForeground(Color.WHITE);
    	  etiquetaVersión.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 18));
    	  etiquetaVersión.setBounds(853, 453, 131, 31);
    	  add(etiquetaVersión);
    	  
    	  JButton botonRegistro = new JButton("REGISTRO");
    	  botonRegistro.addMouseListener(new MouseAdapter() {
    	  	@Override
    	  	public void mouseClicked(MouseEvent arg0) {
    	  		ventana.irPantallaRegistro();
    	  	}
    	  });
    	  botonRegistro.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
    	  botonRegistro.addActionListener(new ActionListener() {
    	  	public void actionPerformed(ActionEvent arg0) {
    	  	}
    	  });
    	  botonRegistro.setBounds(304, 197, 393, 37);
    	  add(botonRegistro);
    	  
    	  JButton botonLogin = new JButton("LOGIN");
    	  botonLogin.addMouseListener(new MouseAdapter() {
    	  	@Override
    	  	public void mouseClicked(MouseEvent e) {
    	  		ventana.irPantallaLogin();
    	  	}
    	  });
    	  botonLogin.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
    	  botonLogin.setBounds(304, 364, 393, 37);
    	  add(botonLogin);
    	  
    	  JLabel etiquetaFondoInicio = new JLabel("");
    	  etiquetaFondoInicio.setBounds(0, 0, 1008,536);
    	  etiquetaFondoInicio.setIcon(new ImageIcon("./imagenes/fondo_inicio.jpg"));
    	  add(etiquetaFondoInicio);
  		  ventana=v;
  		  
  		
  		
  	    
      }
}
