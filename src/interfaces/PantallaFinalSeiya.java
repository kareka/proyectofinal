package interfaces;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;

public class PantallaFinalSeiya extends JPanel{
	private Ventana ventana;
	
    public  PantallaFinalSeiya(Ventana v) {
  	  super();
  	  setSize(new Dimension(1008, 536));
  	  ventana=v;
  	  setBackground(new Color(153, 153, 255));
  	  setLayout(null); 
    
  	  JTextPane panelSeiya = new JTextPane();
	  panelSeiya.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
	  panelSeiya.setBounds(12, 13, 500, 278);
	  panelSeiya.setForeground(Color.WHITE);
  	  panelSeiya.setOpaque(false);
  	  panelSeiya.setText("Despu�s de la batalla en el palacio de Poseid�n, "
  	  		+ "Seiya es liberado de sus obligaciones por Athenea. "
  	  		+ "A partir de ahora podr� buscar a su hermana Seika, primera pista...Grecia.");
	  add(panelSeiya);
	  
	  JButton btnNewButton = new JButton("SALIR");
	  btnNewButton.addMouseListener(new MouseAdapter() {
	  	@Override
	  	public void mouseClicked(MouseEvent arg0) {
	  		System.exit(0);
	  	}
	  });
	  btnNewButton.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
	  btnNewButton.setBounds(860, 441, 97, 25);
	  add(btnNewButton);
    
    
	  JLabel etiquetaFondo = new JLabel("");
	  etiquetaFondo.setBounds(0, 0, 1008, 536);
	  etiquetaFondo.setIcon(new ImageIcon("./imagenes/finalSeiya.jpg"));
	  add(etiquetaFondo);
    }
}
