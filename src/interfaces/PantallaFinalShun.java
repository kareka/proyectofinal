package interfaces;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;

public class PantallaFinalShun  extends JPanel{
	private Ventana ventana;
	
    public  PantallaFinalShun(Ventana v) {
  	  super();
  	  setSize(new Dimension(1008, 536));
  	  ventana=v;
  	  setBackground(new Color(153, 153, 255));
  	  setLayout(null); 
  	  
  	  JTextPane panelShun = new JTextPane();
	  panelShun.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
	  panelShun.setBounds(12, 13, 500, 278);
	  panelShun.setForeground(Color.WHITE);
  	  panelShun.setOpaque(false);
  	  panelShun.setText("Como consecuencia de los �ltimos combates, Shun consigue "
  	  		+ "reencontrarse con Ikki, el cu�l ha cambiado su forma de ser y "
  	  		+ "vuelve a comportarse como lo que es...su HERMANO MAYOR.");
	  add(panelShun);
	  
	  JButton btnNewButton = new JButton("SALIR");
	  btnNewButton.addMouseListener(new MouseAdapter() {
	  	@Override
	  	public void mouseClicked(MouseEvent arg0) {
	  		System.exit(0);
	  	}
	  });
	  btnNewButton.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
	  btnNewButton.setBounds(860, 441, 97, 25);
	  add(btnNewButton);
    
    
	  JLabel etiquetaFondo = new JLabel("");
	  etiquetaFondo.setBounds(0, 0, 1008, 536);
	  etiquetaFondo.setIcon(new ImageIcon("./imagenes/finalShun.jpg"));
	  add(etiquetaFondo);
    
    }
}

