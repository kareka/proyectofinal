package interfaces;

import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PantallaElegirEquipo extends JPanel{
	private Ventana ventana;
	public PantallaElegirEquipo(Ventana v) {
		super();
		this.ventana = ventana;
		setSize(new Dimension(1008, 536));
		setBackground(new Color(153, 153, 255));
		setLayout(null);
		
		JButton botonPatriarca = new JButton("EQUIPO PATRIARCA");
		botonPatriarca.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				v.irPantallaBatallaOro();
			}
		});
		botonPatriarca.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 16));
		botonPatriarca.setBounds(142, 129, 223, 25);
		add(botonPatriarca);				
		
		JButton botonAthenea = new JButton("EQUIPO ATHENEA");
		botonAthenea.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				v.irPantallaBatallaBronce();
			}
		});
		botonAthenea.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 16));
		botonAthenea.setBounds(652, 129, 223, 25);
		add(botonAthenea);
		
		JButton btnEquipoPoseidon = new JButton("EQUIPO POSEIDON");
		btnEquipoPoseidon.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				v.irPantallaBatallaPoseidon();
			}
		});
		btnEquipoPoseidon.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 16));
		btnEquipoPoseidon.setBounds(142, 394, 223, 25);
		add(btnEquipoPoseidon);
		
		JButton btnEquipoHilda = new JButton("EQUIPO HILDA");
		btnEquipoHilda.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				v.irPantallaBatallaHilda();
			}
		});
		btnEquipoHilda.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 16));
		btnEquipoHilda.setBounds(652, 394, 223, 25);
		add(btnEquipoHilda);
		
		JLabel etiquetaFondo = new JLabel("");
		etiquetaFondo.setBounds(0, 0, 1008, 536);
		etiquetaFondo.setIcon(new ImageIcon("./imagenes/simbolos/fondo_equipos_elegir.jpg"));
		add(etiquetaFondo);
		
		
		
	
	}
}
