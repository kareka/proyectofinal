/*
-- Query: SELECT * FROM cabzodiaco.caballero
LIMIT 0, 1000

-- Date: 2019-05-31 11:11
*/
INSERT INTO `caballero` (`nombre`,`armadura`,`procedencia`,`vida`,`cosmos`,`escenario_nombre`) VALUES ('Aiora','oro','Grecia',120,120,'santuario');
INSERT INTO `caballero` (`nombre`,`armadura`,`procedencia`,`vida`,`cosmos`,`escenario_nombre`) VALUES ('Aldebaran','oro','Brasil',120,120,'santuario');
INSERT INTO `caballero` (`nombre`,`armadura`,`procedencia`,`vida`,`cosmos`,`escenario_nombre`) VALUES ('Baian','escamas','Canada',200,200,'poseidon');
INSERT INTO `caballero` (`nombre`,`armadura`,`procedencia`,`vida`,`cosmos`,`escenario_nombre`) VALUES ('Fenrir','zafiro','Noruega',150,150,'asgard');
INSERT INTO `caballero` (`nombre`,`armadura`,`procedencia`,`vida`,`cosmos`,`escenario_nombre`) VALUES ('Hagen','zafiro','Noruega',150,150,'asgard');
INSERT INTO `caballero` (`nombre`,`armadura`,`procedencia`,`vida`,`cosmos`,`escenario_nombre`) VALUES ('Ikki','bronce','Ecuador',100,100,'torneo');
INSERT INTO `caballero` (`nombre`,`armadura`,`procedencia`,`vida`,`cosmos`,`escenario_nombre`) VALUES ('Isaac','escamas','Finlandia',200,200,'poseidon');
INSERT INTO `caballero` (`nombre`,`armadura`,`procedencia`,`vida`,`cosmos`,`escenario_nombre`) VALUES ('Kanon','escamas','Grecia',200,200,'poseidon');
INSERT INTO `caballero` (`nombre`,`armadura`,`procedencia`,`vida`,`cosmos`,`escenario_nombre`) VALUES ('Kishna','escamas','India',200,200,'poseidon');
INSERT INTO `caballero` (`nombre`,`armadura`,`procedencia`,`vida`,`cosmos`,`escenario_nombre`) VALUES ('Milo','oro','Grecia',120,120,'santuario');
INSERT INTO `caballero` (`nombre`,`armadura`,`procedencia`,`vida`,`cosmos`,`escenario_nombre`) VALUES ('Mu','oro','Tibet',120,120,'santuario');
INSERT INTO `caballero` (`nombre`,`armadura`,`procedencia`,`vida`,`cosmos`,`escenario_nombre`) VALUES ('Saga','oro','Grecia',120,120,'santuario');
INSERT INTO `caballero` (`nombre`,`armadura`,`procedencia`,`vida`,`cosmos`,`escenario_nombre`) VALUES ('Seiya','bronce','Grecia',100,100,'torneo');
INSERT INTO `caballero` (`nombre`,`armadura`,`procedencia`,`vida`,`cosmos`,`escenario_nombre`) VALUES ('Shiryu','bronce','China',100,100,'torneo');
INSERT INTO `caballero` (`nombre`,`armadura`,`procedencia`,`vida`,`cosmos`,`escenario_nombre`) VALUES ('Shun','bronce','Etiopia',100,100,'torneo');
INSERT INTO `caballero` (`nombre`,`armadura`,`procedencia`,`vida`,`cosmos`,`escenario_nombre`) VALUES ('Sigfried','zafiro','Noruega',150,150,'asgard');
INSERT INTO `caballero` (`nombre`,`armadura`,`procedencia`,`vida`,`cosmos`,`escenario_nombre`) VALUES ('Sorrento','escamas','Austria',200,200,'poseidon');
INSERT INTO `caballero` (`nombre`,`armadura`,`procedencia`,`vida`,`cosmos`,`escenario_nombre`) VALUES ('Syd','zafiro','Noruega',150,150,'asgard');
INSERT INTO `caballero` (`nombre`,`armadura`,`procedencia`,`vida`,`cosmos`,`escenario_nombre`) VALUES ('Thor','zafiro','Noruega',150,150,'asgard');
INSERT INTO `caballero` (`nombre`,`armadura`,`procedencia`,`vida`,`cosmos`,`escenario_nombre`) VALUES ('Yoga','bronce','Rusia',100,100,'torneo');
