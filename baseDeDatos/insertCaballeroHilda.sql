/*
-- Query: SELECT * FROM cabzodiaco.caballerohilda
LIMIT 0, 1000

-- Date: 2019-05-31 11:12
*/
INSERT INTO `caballerohilda` (`mitologia`,`caballero_nombre`) VALUES ('Epsilon','Fenrir');
INSERT INTO `caballerohilda` (`mitologia`,`caballero_nombre`) VALUES ('Beta','Hagen');
INSERT INTO `caballerohilda` (`mitologia`,`caballero_nombre`) VALUES ('Alpha','Sigfried');
INSERT INTO `caballerohilda` (`mitologia`,`caballero_nombre`) VALUES ('Zeta','Syd');
INSERT INTO `caballerohilda` (`mitologia`,`caballero_nombre`) VALUES ('Gamma','Thor');
