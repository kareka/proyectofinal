/*
-- Query: SELECT * FROM cabzodiaco.caballerooro
LIMIT 0, 1000

-- Date: 2019-05-31 11:12
*/
INSERT INTO `caballerooro` (`casa`,`caballero_nombre`) VALUES ('Leo','Aiora');
INSERT INTO `caballerooro` (`casa`,`caballero_nombre`) VALUES ('Tauro','Aldebaran');
INSERT INTO `caballerooro` (`casa`,`caballero_nombre`) VALUES ('Escorpio','Milo');
INSERT INTO `caballerooro` (`casa`,`caballero_nombre`) VALUES ('Aries','Mu');
INSERT INTO `caballerooro` (`casa`,`caballero_nombre`) VALUES ('Geminis','Saga');
