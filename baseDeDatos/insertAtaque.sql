/*
-- Query: SELECT * FROM cabzodiaco.ataque
LIMIT 0, 1000

-- Date: 2019-05-31 11:11
*/
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Aguja de fuego','poder',30,'30','Milo');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Aguja escarlata','golpe',20,'20','Milo');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Alas ardientes','poder',30,'30','Ikki');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Ataque de la jauria','final',50,'50','Fenrir');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Aurora boreal','golpe',20,'20','Isaac');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Barrera de chakra','final',50,'50','Kishna');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Barrera protectora','final',50,'50','Baian');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Cadena nebular','poder',30,'30','Shun');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Choque giratorio','poder',30,'30','Seiya');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Climax mortal','final',50,'50','Sorrento');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Cometa de pegaso','final',50,'50','Seiya');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Congelacion del universo','poder',30,'30','Hagen');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Cosmos fulminante','poder',30,'30','Saga');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Defensa giratoria','final',50,'50','Shun');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Elevacion del dragon','final',50,'50','Shiryu');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Embestida del toro','poder',30,'30','Aldebaran');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Energia dorada','final',50,'50','Aiora');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Esfera de energia','final',50,'50','Milo');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Espada de Odin','poder',30,'30','Sigfried');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Espiral','final',50,'50','Mu');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Excalibur','golpe',20,'20','Shiryu');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Explosion de galaxias','final',50,'50','Saga');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Explosion galactica','final',50,'50','Kanon');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Flauta dulce','golpe',20,'20','Sorrento');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Garra de tigre vikingo','golpe',20,'20','Syd');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Garra feroz','golpe',20,'20','Fenrir');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Gran cuerno','final',50,'50','Aldebaran');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Gran presion ardiente','final',50,'50','Hagen');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Heracles titanico','poder',30,'30','Thor');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Ilusion diabolica','final',50,'50','Ikki');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Impulso azul','final',50,'50','Syd');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Kens de energia','poder',30,'30','Aiora');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Lanzada destelleante','golpe',20,'20','Kishna');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Levitacion','poder',30,'30','Kishna');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Lobo de la estepa','poder',30,'30','Fenrir');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Martillo de Mjolnir','golpe',20,'20','Thor');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Meteoro de pegaso','golpe',20,'20','Seiya');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Muro de cristal','golpe',20,'20','Mu');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Nebulosa de andromeda','golpe',20,'20','Shun');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Oleadas ascendentes','golpe',20,'20','Baian');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Otra dimension','poder',30,'30','Kanon');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Poder de la estrella Alpha','golpe',20,'20','Sigfried');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Polvo de diamante','final',50,'50','Yoga');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Polvo de diamantes extremo','poder',30,'30','Isaac');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Puño de hielo','golpe',20,'20','Yoga');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Puño de hierro','golpe',20,'20','Aldebaran');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Puño de leo','golpe',20,'20','Aiora');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Puño de luz','golpe',20,'20','Saga');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Puño de titan','final',50,'50','Thor');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Puño fantasma','golpe',20,'20','Ikki');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Rayo de fuego','golpe',20,'20','Hagen');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Rayo de la aurora','poder',30,'30','Yoga');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Red de cristal','poder',30,'30','Mu');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Sinfonia mortal','poder',30,'30','Sorrento');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Sombra del tigre','poder',30,'30','Syd');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Soplo divino','poder',30,'30','Baian');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Torbellino polar','final',50,'50','Isaac');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Triangulo dorado','golpe',20,'20','Kanon');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Ventisca de dragon','final',50,'50','Sigfried');
INSERT INTO `ataque` (`nombre`,`tipo`,`nivelDano`,`gastoCosmos`,`caballero_nombre`) VALUES ('Vuelo del dragon','poder',30,'30','Shiryu');
